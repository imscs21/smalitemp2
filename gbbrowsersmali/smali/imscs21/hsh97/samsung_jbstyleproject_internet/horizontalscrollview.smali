.class public Limscs21/hsh97/samsung_jbstyleproject_internet/horizontalscrollview;
.super Landroid/widget/HorizontalScrollView;
.source "horizontalscrollview.java"


# static fields
.field static final Developer:Ljava/lang/String; = "imscs21(=Develious=imscsc21=Mr.Hwang"

.field static final Developer_Blog:Ljava/lang/String; = "Developer Blog: http://hsh97.tistory.com"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "context"

    .prologue
    .line 9
    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 10
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    return-void
.end method


# virtual methods
.method public onAttachedToWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 23
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Limscs21/hsh97/samsung_jbstyleproject_internet/horizontalscrollview;->setOverScrollMode(I)V

    .line 24
    invoke-virtual {p0, v1}, Limscs21/hsh97/samsung_jbstyleproject_internet/horizontalscrollview;->setScrollbarFadingEnabled(Z)V

    .line 25
    invoke-virtual {p0, v1}, Limscs21/hsh97/samsung_jbstyleproject_internet/horizontalscrollview;->setHorizontalScrollBarEnabled(Z)V

    .line 26
    invoke-virtual {p0, v1}, Limscs21/hsh97/samsung_jbstyleproject_internet/horizontalscrollview;->setHorizontalFadingEdgeEnabled(Z)V

    .line 27
    invoke-virtual {p0, v1}, Limscs21/hsh97/samsung_jbstyleproject_internet/horizontalscrollview;->setVerticalScrollBarEnabled(Z)V

    .line 29
    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->onAttachedToWindow()V

    .line 31
    return-void
.end method
