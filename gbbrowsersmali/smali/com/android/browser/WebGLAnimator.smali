.class Lcom/android/browser/WebGLAnimator;
.super Ljava/lang/Object;
.source "WebGLAnimator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/browser/WebGLAnimator$ScaleDetectorListener;
    }
.end annotation


# static fields
.field private static ANIMATION_SCALE:I

.field private static final BOUNCE_ANIMATION_MS:I

.field private static final DBG:Z

.field private static final FLING_BOUNCE_ANIMATION_MS:I

.field private static final FLING_EXTRA_ANIMATION_MS:I

.field private static final MAX_HIDE_LOCKSCREEN_RESIZE_MS:I

.field private static final MAX_HIDE_LOCKSCREEN_SAME_SIZE_MS:I

.field private static MINIMUM_SCALE_INCREMENT:F

.field private static final MIN_HIDE_LOCKSCREEN_RESIZE_MS:I

.field private static final MIN_HIDE_LOCKSCREEN_SAME_SIZE_MS:I

.field private static final QUICK_BOUNCE_ANIMATION_MS:I

.field private static final ZOOM_ANIMATION_MS:I

.field private static final ZOOM_BOUNCE_ANIMATION_MS:I


# instance fields
.field bFlingToEdge:Z

.field private mAbortingAnimation:Z

.field mActiveTabsPageGestureDetector:Lcom/android/browser/ActiveTabsPageGestureDetector;

.field private mBouncePeriodMs:J

.field private mBounceStartMs:J

.field private mBounceStartPos:[I

.field private mBounceStartZoomScale:F

.field private mBounceTargetPos:[I

.field private mBounceTargetZoomScale:F

.field private mBrowserActivity:Lcom/android/browser/BrowserActivity;

.field mCenter:[F

.field mCheckScrollforEdge:Z

.field private mContext:Landroid/content/Context;

.field private mDTBounceBack:Z

.field private mDTTargetZoomScale:F

.field private mDTZoomDuration:J

.field private mDTZoomStartMs:J

.field private mDisplayHeight:I

.field private mDisplayWidth:I

.field private mDrawingLastFrame:Z

.field mEdgeAnimating:I

.field mEdgeToAnimate:I

.field private mFilteredPointerPos:[I

.field private mFirstTouchPosX:F

.field private mFirstTouchPosY:F

.field mFlingBounceStartMs:[J

.field mFlingEdgeGlowStartMs:[J

.field private mFlingExtra:[I

.field private mFlingFinal:[I

.field private mFlingOOBStartMs:[J

.field private mFlingTarget:[I

.field mGLHasPaused:Z

.field volatile mGLWakeupTime:J

.field volatile mGLWakeupTimeMin:J

.field mGlThreadID:I

.field private mHasPinched:Z

.field mHideLockedStartMs:J

.field private mIsPinchCenterInited:Z

.field private mLastPointerPos:[I

.field private mLastTrackMoveX:I

.field private mLastTrackMoveY:I

.field private mLastUIMovePos:[I

.field private mLastVelX:F

.field private mLastVelY:F

.field private mLastVelocity:F

.field mLastWebViewDrawFinishedMs:J

.field private mLocationOnScreen:[I

.field private mMaxScale:F

.field private mMaximumFling:I

.field private mMinLockSnapReverseDistance:I

.field private mMinScale:F

.field private mMotionFilter:Lcom/android/browser/MotionFilter;

.field mNewScroll:[F

.field mNewScrollInt:[I

.field mNewScrollMax:[I

.field private mPinchFrames:J

.field private mPinchMotionFilter:Lcom/android/browser/MotionFilter;

.field private mPointerDevice:Landroid/view/PointerDevice;

.field private mPointerPos:[I

.field private mPrintFirstPinch:Z

.field private mScaleDetector:Lcom/android/browser/DirectScaleGestureDetector;

.field private mScrollFrames:J

.field private mScrollMax:[I

.field private mScrollMin:[I

.field private mScrollPos:[I

.field private mScrollStart:[I

.field private mScrollStartMs:J

.field private mScrollStartScale:F

.field private mScroller:Landroid/widget/Scroller;

.field private mSecondPointerDownMs:J

.field private mSnapPositive:Z

.field private mSnapScrollMode:I

.field private mState:I

.field private mStates:[Landroid/view/PointerDevice$PointerState;

.field private mStationary:Z

.field private mStationaryPointerPos:[I

.field private mTargetZoomScale:F

.field private mTouchDown:[I

.field private mTouchDownMs:J

.field private mTouchDownStale:Z

.field private mTouchSlop:I

.field mVariableEdgeWidth:I

.field mVelocityTracker:Landroid/view/VelocityTracker;

.field mVelocityTrackerQuickFlick:Landroid/view/VelocityTracker;

.field private mWebView:Landroid/webkit/WebView;

.field mWebViewDone:Z

.field private mZoomCenterX:F

.field private mZoomCenterY:F

.field private mZoomScale:F

.field private mZoomView:Lcom/android/browser/BitmapWebView;

.field private stateNames:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    const-string v0, "1"

    const-string v1, "debug.browser.animation"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/browser/WebGLAnimator;->DBG:Z

    .line 46
    const/4 v0, 0x1

    sput v0, Lcom/android/browser/WebGLAnimator;->ANIMATION_SCALE:I

    .line 49
    :try_start_0
    const-string v0, "debug.browser.animation_scale"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/browser/WebGLAnimator;->ANIMATION_SCALE:I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    :goto_0
    sget v0, Lcom/android/browser/WebGLAnimator;->ANIMATION_SCALE:I

    mul-int/lit16 v0, v0, 0x12c

    sput v0, Lcom/android/browser/WebGLAnimator;->BOUNCE_ANIMATION_MS:I

    .line 71
    sget v0, Lcom/android/browser/WebGLAnimator;->ANIMATION_SCALE:I

    mul-int/lit8 v0, v0, 0x64

    sput v0, Lcom/android/browser/WebGLAnimator;->QUICK_BOUNCE_ANIMATION_MS:I

    .line 72
    sget v0, Lcom/android/browser/WebGLAnimator;->ANIMATION_SCALE:I

    mul-int/lit16 v0, v0, 0x118

    sput v0, Lcom/android/browser/WebGLAnimator;->ZOOM_ANIMATION_MS:I

    .line 73
    sget v0, Lcom/android/browser/WebGLAnimator;->ANIMATION_SCALE:I

    mul-int/lit8 v0, v0, 0x6e

    sput v0, Lcom/android/browser/WebGLAnimator;->ZOOM_BOUNCE_ANIMATION_MS:I

    .line 74
    sget v0, Lcom/android/browser/WebGLAnimator;->ANIMATION_SCALE:I

    mul-int/lit16 v0, v0, 0xb4

    sput v0, Lcom/android/browser/WebGLAnimator;->FLING_EXTRA_ANIMATION_MS:I

    .line 75
    sget v0, Lcom/android/browser/WebGLAnimator;->ANIMATION_SCALE:I

    mul-int/lit16 v0, v0, 0x1f4

    sput v0, Lcom/android/browser/WebGLAnimator;->FLING_BOUNCE_ANIMATION_MS:I

    .line 79
    sget v0, Lcom/android/browser/WebGLAnimator;->ANIMATION_SCALE:I

    mul-int/lit8 v0, v0, 0x0

    sput v0, Lcom/android/browser/WebGLAnimator;->MIN_HIDE_LOCKSCREEN_SAME_SIZE_MS:I

    .line 80
    sget v0, Lcom/android/browser/WebGLAnimator;->ANIMATION_SCALE:I

    mul-int/lit8 v0, v0, 0x0

    sput v0, Lcom/android/browser/WebGLAnimator;->MAX_HIDE_LOCKSCREEN_SAME_SIZE_MS:I

    .line 81
    sget v0, Lcom/android/browser/WebGLAnimator;->ANIMATION_SCALE:I

    mul-int/lit8 v0, v0, 0x0

    sput v0, Lcom/android/browser/WebGLAnimator;->MIN_HIDE_LOCKSCREEN_RESIZE_MS:I

    .line 82
    sget v0, Lcom/android/browser/WebGLAnimator;->ANIMATION_SCALE:I

    mul-int/lit8 v0, v0, 0x0

    sput v0, Lcom/android/browser/WebGLAnimator;->MAX_HIDE_LOCKSCREEN_RESIZE_MS:I

    .line 221
    const v0, 0x3c23d70a

    sput v0, Lcom/android/browser/WebGLAnimator;->MINIMUM_SCALE_INCREMENT:F

    return-void

    .line 50
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/browser/BitmapWebView;)V
    .locals 10
    .parameter "context"
    .parameter "zoomView"

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x2

    .line 245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    iput v7, p0, Lcom/android/browser/WebGLAnimator;->mSnapScrollMode:I

    .line 139
    const/4 v3, 0x7

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "NONE"

    aput-object v4, v3, v7

    const-string v4, "ONE_DOWN"

    aput-object v4, v3, v8

    const-string v4, "SCROLL"

    aput-object v4, v3, v6

    const/4 v4, 0x3

    const-string v5, "FLING"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "PINCH"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const-string v5, "ZOOM"

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "BOUNCE"

    aput-object v5, v3, v4

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->stateNames:[Ljava/lang/String;

    .line 168
    iput v8, p0, Lcom/android/browser/WebGLAnimator;->mState:I

    .line 172
    new-instance v3, Lcom/android/browser/MotionFilter;

    invoke-direct {v3}, Lcom/android/browser/MotionFilter;-><init>()V

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mMotionFilter:Lcom/android/browser/MotionFilter;

    .line 173
    new-instance v3, Lcom/android/browser/MotionFilter;

    invoke-direct {v3}, Lcom/android/browser/MotionFilter;-><init>()V

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mPinchMotionFilter:Lcom/android/browser/MotionFilter;

    .line 175
    new-array v3, v6, [I

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mLocationOnScreen:[I

    .line 176
    new-array v3, v6, [I

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mPointerPos:[I

    .line 177
    new-array v3, v6, [I

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mStationaryPointerPos:[I

    .line 178
    new-array v3, v6, [I

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mLastPointerPos:[I

    .line 179
    new-array v3, v6, [I

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mFilteredPointerPos:[I

    .line 180
    new-array v3, v6, [I

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mTouchDown:[I

    .line 181
    new-array v3, v6, [I

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mLastUIMovePos:[I

    .line 182
    new-array v3, v6, [I

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollStart:[I

    .line 183
    new-array v3, v6, [I

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    .line 184
    new-array v3, v6, [I

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    .line 185
    new-array v3, v6, [I

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    .line 187
    new-array v3, v6, [I

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mBounceStartPos:[I

    .line 188
    new-array v3, v6, [I

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mBounceTargetPos:[I

    .line 189
    new-array v3, v6, [I

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mFlingTarget:[I

    .line 190
    new-array v3, v6, [I

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mFlingFinal:[I

    .line 191
    new-array v3, v6, [I

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mFlingExtra:[I

    .line 192
    new-array v3, v6, [J

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mFlingOOBStartMs:[J

    .line 202
    iput v9, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterX:F

    iput v9, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterY:F

    .line 215
    const/high16 v3, 0x3f80

    iput v3, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    .line 816
    new-array v3, v6, [F

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mCenter:[F

    .line 817
    new-array v3, v6, [F

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mNewScroll:[F

    .line 818
    new-array v3, v6, [I

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollInt:[I

    .line 819
    new-array v3, v6, [I

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollMax:[I

    .line 1181
    iput v7, p0, Lcom/android/browser/WebGLAnimator;->mEdgeAnimating:I

    .line 1182
    iput v7, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    .line 1183
    const/16 v3, 0xa

    iput v3, p0, Lcom/android/browser/WebGLAnimator;->mVariableEdgeWidth:I

    .line 1184
    iput-boolean v7, p0, Lcom/android/browser/WebGLAnimator;->bFlingToEdge:Z

    .line 1185
    iput-boolean v7, p0, Lcom/android/browser/WebGLAnimator;->mCheckScrollforEdge:Z

    .line 1421
    iput-boolean v8, p0, Lcom/android/browser/WebGLAnimator;->mWebViewDone:Z

    .line 1827
    new-array v3, v6, [J

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mFlingBounceStartMs:[J

    .line 1829
    new-array v3, v6, [J

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mFlingEdgeGlowStartMs:[J

    .line 2388
    const/4 v3, -0x1

    iput v3, p0, Lcom/android/browser/WebGLAnimator;->mGlThreadID:I

    .line 246
    iput-object p1, p0, Lcom/android/browser/WebGLAnimator;->mContext:Landroid/content/Context;

    .line 247
    iput-object p2, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    .line 249
    new-instance v3, Lcom/android/browser/ActiveTabsPageGestureDetector;

    invoke-direct {v3, p1, p2}, Lcom/android/browser/ActiveTabsPageGestureDetector;-><init>(Landroid/content/Context;Lcom/android/browser/BitmapWebView;)V

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mActiveTabsPageGestureDetector:Lcom/android/browser/ActiveTabsPageGestureDetector;

    .line 251
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 252
    .local v0, configuration:Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    .line 254
    .local v2, slop:I
    iput v2, p0, Lcom/android/browser/WebGLAnimator;->mTouchSlop:I

    .line 256
    iput v2, p0, Lcom/android/browser/WebGLAnimator;->mMinLockSnapReverseDistance:I

    .line 258
    new-instance v3, Landroid/widget/Scroller;

    invoke-direct {v3, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScroller:Landroid/widget/Scroller;

    .line 259
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v3

    iput v3, p0, Lcom/android/browser/WebGLAnimator;->mMaximumFling:I

    .line 261
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 262
    .local v1, displayMetrics:Landroid/util/DisplayMetrics;
    iget v3, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v3, p0, Lcom/android/browser/WebGLAnimator;->mDisplayWidth:I

    .line 263
    iget v3, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v3, p0, Lcom/android/browser/WebGLAnimator;->mDisplayHeight:I

    .line 264
    check-cast p1, Lcom/android/browser/BrowserActivity;

    .end local p1
    iput-object p1, p0, Lcom/android/browser/WebGLAnimator;->mBrowserActivity:Lcom/android/browser/BrowserActivity;

    .line 265
    return-void
.end method

.method private _setWebViewScrollAndZoom(ZZ)Z
    .locals 13
    .parameter
    .parameter

    .prologue
    const/4 v11, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const-string v12, ","

    const-string v10, ", "

    .line 854
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    .line 858
    if-eqz p1, :cond_f

    .line 859
    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mMinScale:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_e

    .line 860
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mMinScale:F

    move v1, v0

    move v0, v9

    .line 863
    :goto_0
    iget v2, p0, Lcom/android/browser/WebGLAnimator;->mMaxScale:F

    cmpl-float v2, v1, v2

    if-lez v2, :cond_0

    .line 864
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mMaxScale:F

    move v1, v0

    move v0, v9

    .line 869
    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mCenter:[F

    iget v3, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterX:F

    aput v3, v2, v8

    .line 870
    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mCenter:[F

    iget v3, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterY:F

    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    invoke-virtual {v4}, Lcom/android/browser/BitmapWebView;->getTitleBarHeight()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    aput v3, v2, v9

    .line 872
    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getScale()F

    move-result v2

    mul-float/2addr v2, v1

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v3}, Landroid/webkit/WebView;->getContentWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 873
    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v3}, Landroid/webkit/WebView;->getScale()F

    move-result v3

    mul-float/2addr v3, v1

    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v4}, Landroid/webkit/WebView;->getContentHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 874
    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollMax:[I

    iget-object v5, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->getWidth()I

    move-result v5

    sub-int v5, v2, v5

    invoke-static {v8, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    aput v5, v4, v8

    .line 875
    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollMax:[I

    iget-object v5, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->getHeight()I

    move-result v5

    sub-int v5, v3, v5

    iget-object v6, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v6}, Landroid/webkit/WebView;->getTitleHeight()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v8, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    aput v5, v4, v9

    move v4, v8

    .line 877
    :goto_2
    if-ge v4, v11, :cond_1

    .line 878
    iget-object v5, p0, Lcom/android/browser/WebGLAnimator;->mNewScroll:[F

    iget-object v6, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v6, v6, v4

    int-to-float v6, v6

    iget-object v7, p0, Lcom/android/browser/WebGLAnimator;->mCenter:[F

    aget v7, v7, v4

    add-float/2addr v6, v7

    iget-object v7, p0, Lcom/android/browser/WebGLAnimator;->mCenter:[F

    aget v7, v7, v4

    div-float/2addr v7, v1

    sub-float/2addr v6, v7

    aput v6, v5, v4

    .line 879
    iget-object v5, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollInt:[I

    iget-object v6, p0, Lcom/android/browser/WebGLAnimator;->mNewScroll:[F

    aget v6, v6, v4

    mul-float/2addr v6, v1

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    aput v6, v5, v4

    .line 877
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 882
    :cond_1
    sget-boolean v4, Lcom/android/browser/WebGLAnimator;->DBG:Z

    if-eqz v4, :cond_2

    .line 883
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "BOUNCE: contentW/H    = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->getContentWidth()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->getContentHeight()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 884
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "BOUNCE: newWebViewW/H = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ","

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 885
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BOUNCE: newScale      = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 886
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BOUNCE: newScale(WV)  = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v3}, Landroid/webkit/WebView;->getScale()F

    move-result v3

    mul-float/2addr v3, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 887
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BOUNCE: oldScroll     = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v3, v3, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v3, v3, v9

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 888
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BOUNCE: newScroll     = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollInt:[I

    aget v3, v3, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollInt:[I

    aget v3, v3, v9

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 889
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BOUNCE: oldMax        = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v3, v3, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v3, v3, v9

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 890
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BOUNCE: newMax        = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollMax:[I

    aget v3, v3, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollMax:[I

    aget v3, v3, v9

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 893
    :cond_2
    if-eqz p1, :cond_8

    move v2, v0

    move v0, v8

    .line 894
    :goto_3
    if-ge v0, v11, :cond_5

    .line 895
    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollInt:[I

    aget v3, v3, v0

    if-gez v3, :cond_4

    .line 897
    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v3, v3, v0

    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mNewScroll:[F

    aget v4, v4, v0

    float-to-int v4, v4

    sub-int/2addr v3, v4

    aput v3, v2, v0

    move v2, v9

    .line 894
    :cond_3
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 898
    :cond_4
    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollInt:[I

    aget v3, v3, v0

    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollMax:[I

    aget v4, v4, v0

    if-le v3, v4, :cond_3

    .line 900
    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollInt:[I

    aget v2, v2, v0

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollMax:[I

    aget v3, v3, v0

    sub-int/2addr v2, v3

    int-to-float v2, v2

    .line 901
    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v4, v4, v0

    div-float/2addr v2, v1

    float-to-int v2, v2

    sub-int v2, v4, v2

    aput v2, v3, v0

    move v2, v9

    goto :goto_4

    .line 905
    :cond_5
    sget-boolean v0, Lcom/android/browser/WebGLAnimator;->DBG:Z

    if-eqz v0, :cond_6

    .line 906
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setWebViewScrollAndZoom: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->getConfigString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 909
    :cond_6
    if-eqz v2, :cond_7

    move v0, v8

    .line 936
    :goto_5
    return v0

    :cond_7
    move v0, v9

    .line 912
    goto :goto_5

    .line 915
    :cond_8
    sget-boolean v0, Lcom/android/browser/WebGLAnimator;->DBG:Z

    if-eqz v0, :cond_9

    .line 916
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setWebViewScrollAndZoom: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->getConfigString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    :cond_9
    move v0, v8

    .line 920
    :goto_6
    if-ge v0, v11, :cond_c

    .line 921
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollInt:[I

    aget v1, v1, v0

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollMax:[I

    aget v2, v2, v0

    if-le v1, v2, :cond_a

    .line 922
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollInt:[I

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollMax:[I

    aget v2, v2, v0

    aput v2, v1, v0

    .line 924
    :cond_a
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollInt:[I

    aget v1, v1, v0

    if-gez v1, :cond_b

    .line 925
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollInt:[I

    aput v8, v1, v0

    .line 920
    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 929
    :cond_c
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getScale()F

    move-result v2

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollInt:[I

    aget v2, v2, v8

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollInt:[I

    aget v3, v3, v9

    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mCenter:[F

    aget v4, v4, v8

    iget-object v5, p0, Lcom/android/browser/WebGLAnimator;->mCenter:[F

    aget v5, v5, v9

    move v6, p2

    invoke-virtual/range {v0 .. v6}, Landroid/webkit/WebView;->setScrollAndZoom(FIIFFZ)V

    .line 932
    sget-boolean v0, Lcom/android/browser/WebGLAnimator;->DBG:Z

    if-eqz v0, :cond_d

    .line 933
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "newScroll: = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollInt:[I

    aget v1, v1, v8

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollInt:[I

    aget v1, v1, v9

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " => "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getScrollX()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getScrollY()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    :cond_d
    move v0, v9

    .line 936
    goto/16 :goto_5

    :cond_e
    move v1, v0

    move v0, v8

    goto/16 :goto_0

    :cond_f
    move v1, v0

    move v0, v8

    goto/16 :goto_1
.end method

.method static synthetic access$100(Lcom/android/browser/WebGLAnimator;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 44
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mState:I

    return v0
.end method

.method static synthetic access$1000(Lcom/android/browser/WebGLAnimator;)F
    .locals 1
    .parameter "x0"

    .prologue
    .line 44
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterX:F

    return v0
.end method

.method static synthetic access$1002(Lcom/android/browser/WebGLAnimator;F)F
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 44
    iput p1, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterX:F

    return p1
.end method

.method static synthetic access$1100(Lcom/android/browser/WebGLAnimator;)F
    .locals 1
    .parameter "x0"

    .prologue
    .line 44
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterY:F

    return v0
.end method

.method static synthetic access$1102(Lcom/android/browser/WebGLAnimator;F)F
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 44
    iput p1, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterY:F

    return p1
.end method

.method static synthetic access$1300(Lcom/android/browser/WebGLAnimator;)F
    .locals 1
    .parameter "x0"

    .prologue
    .line 44
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mTargetZoomScale:F

    return v0
.end method

.method static synthetic access$1302(Lcom/android/browser/WebGLAnimator;F)F
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 44
    iput p1, p0, Lcom/android/browser/WebGLAnimator;->mTargetZoomScale:F

    return p1
.end method

.method static synthetic access$1400()Z
    .locals 1

    .prologue
    .line 44
    sget-boolean v0, Lcom/android/browser/WebGLAnimator;->DBG:Z

    return v0
.end method

.method static synthetic access$1500()F
    .locals 1

    .prologue
    .line 44
    sget v0, Lcom/android/browser/WebGLAnimator;->MINIMUM_SCALE_INCREMENT:F

    return v0
.end method

.method static synthetic access$1600(Lcom/android/browser/WebGLAnimator;)F
    .locals 1
    .parameter "x0"

    .prologue
    .line 44
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mMinScale:F

    return v0
.end method

.method static synthetic access$1700(Lcom/android/browser/WebGLAnimator;)F
    .locals 1
    .parameter "x0"

    .prologue
    .line 44
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mMaxScale:F

    return v0
.end method

.method static synthetic access$200(Lcom/android/browser/WebGLAnimator;I)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/android/browser/WebGLAnimator;->changeStateTo(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/android/browser/WebGLAnimator;I)Ljava/lang/String;
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/android/browser/WebGLAnimator;->getStateName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/browser/WebGLAnimator;Ljava/lang/String;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/android/browser/WebGLAnimator;I)Z
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/android/browser/WebGLAnimator;->isState(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/android/browser/WebGLAnimator;)Lcom/android/browser/BitmapWebView;
    .locals 1
    .parameter "x0"

    .prologue
    .line 44
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/browser/WebGLAnimator;Ljava/lang/String;)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/android/browser/WebGLAnimator;->logIfDBG(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/android/browser/WebGLAnimator;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/android/browser/WebGLAnimator;->mIsPinchCenterInited:Z

    return v0
.end method

.method static synthetic access$802(Lcom/android/browser/WebGLAnimator;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/android/browser/WebGLAnimator;->mIsPinchCenterInited:Z

    return p1
.end method

.method static synthetic access$900(Lcom/android/browser/WebGLAnimator;)F
    .locals 1
    .parameter "x0"

    .prologue
    .line 44
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    return v0
.end method

.method private adjustSnapScrollMode()V
    .locals 9

    .prologue
    const/16 v8, 0x10

    const/4 v7, 0x2

    const/4 v3, 0x1

    const/4 v6, 0x0

    const/high16 v5, 0x3fc0

    .line 1492
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mSnapScrollMode:I

    if-eq v0, v7, :cond_0

    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mSnapScrollMode:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 1493
    :cond_0
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mPointerPos:[I

    aget v0, v0, v6

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mLastPointerPos:[I

    aget v1, v1, v6

    sub-int/2addr v0, v1

    .line 1494
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mPointerPos:[I

    aget v1, v1, v3

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mLastPointerPos:[I

    aget v2, v2, v3

    sub-int/2addr v1, v2

    .line 1496
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 1497
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 1498
    iget v4, p0, Lcom/android/browser/WebGLAnimator;->mSnapScrollMode:I

    if-ne v4, v7, :cond_4

    .line 1500
    int-to-float v1, v3

    int-to-float v4, v2

    mul-float/2addr v4, v5

    cmpl-float v1, v1, v4

    if-lez v1, :cond_1

    if-le v3, v8, :cond_1

    .line 1502
    iput v6, p0, Lcom/android/browser/WebGLAnimator;->mSnapScrollMode:I

    .line 1505
    :cond_1
    int-to-float v1, v2

    int-to-float v2, v3

    mul-float/2addr v2, v5

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    iget-boolean v1, p0, Lcom/android/browser/WebGLAnimator;->mSnapPositive:Z

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mMinLockSnapReverseDistance:I

    neg-int v1, v1

    if-ge v0, v1, :cond_2

    .line 1509
    :goto_0
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mSnapScrollMode:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mSnapScrollMode:I

    .line 1526
    :cond_2
    :goto_1
    return-void

    .line 1505
    :cond_3
    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mMinLockSnapReverseDistance:I

    if-le v0, v1, :cond_2

    goto :goto_0

    .line 1513
    :cond_4
    int-to-float v0, v2

    int-to-float v4, v3

    mul-float/2addr v4, v5

    cmpl-float v0, v0, v4

    if-lez v0, :cond_5

    if-le v2, v8, :cond_5

    .line 1515
    iput v6, p0, Lcom/android/browser/WebGLAnimator;->mSnapScrollMode:I

    .line 1518
    :cond_5
    int-to-float v0, v3

    int-to-float v2, v2

    mul-float/2addr v2, v5

    cmpl-float v0, v0, v2

    if-lez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/browser/WebGLAnimator;->mSnapPositive:Z

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mMinLockSnapReverseDistance:I

    neg-int v0, v0

    if-ge v1, v0, :cond_2

    .line 1522
    :goto_2
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mSnapScrollMode:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mSnapScrollMode:I

    goto :goto_1

    .line 1518
    :cond_6
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mMinLockSnapReverseDistance:I

    if-le v1, v0, :cond_2

    goto :goto_2
.end method

.method private animateBounceLocked()Z
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v9, 0x1

    .line 1980
    const-string v0, "Browser"

    const-string v1, "BOUNCE ANIMATION DISABLED shouldn\'t be here!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1983
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/browser/WebGLAnimator;->mBounceStartMs:J

    sub-long/2addr v0, v2

    long-to-int v6, v0

    .line 1984
    int-to-long v0, v6

    iget-wide v2, p0, Lcom/android/browser/WebGLAnimator;->mBouncePeriodMs:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    move v7, v4

    .line 1985
    :goto_0
    const/4 v0, 0x2

    if-ge v7, v0, :cond_0

    .line 1986
    iget-object v8, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    iget-wide v0, p0, Lcom/android/browser/WebGLAnimator;->mBouncePeriodMs:J

    int-to-long v2, v6

    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mBounceStartPos:[I

    aget v4, v4, v7

    iget-object v5, p0, Lcom/android/browser/WebGLAnimator;->mBounceTargetPos:[I

    aget v5, v5, v7

    invoke-static/range {v0 .. v5}, Lcom/android/browser/Kinetic;->getPosDecel(JJII)I

    move-result v0

    aput v0, v8, v7

    .line 1985
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 1988
    :cond_0
    iget-wide v0, p0, Lcom/android/browser/WebGLAnimator;->mBouncePeriodMs:J

    int-to-long v2, v6

    iget v4, p0, Lcom/android/browser/WebGLAnimator;->mBounceStartZoomScale:F

    iget v5, p0, Lcom/android/browser/WebGLAnimator;->mBounceTargetZoomScale:F

    invoke-static/range {v0 .. v5}, Lcom/android/browser/Kinetic;->getScaleDecel(JJFF)F

    move-result v0

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    .line 1998
    :goto_1
    return v9

    .line 1990
    :cond_1
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mBounceTargetPos:[I

    aget v1, v1, v4

    aput v1, v0, v4

    .line 1991
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mBounceTargetPos:[I

    aget v1, v1, v9

    aput v1, v0, v9

    .line 1992
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mBounceTargetZoomScale:F

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    .line 1993
    invoke-direct {p0, v9}, Lcom/android/browser/WebGLAnimator;->requestStateChange(I)V

    goto :goto_1
.end method

.method private animateFlingLocked()Z
    .locals 13

    .prologue
    .line 1833
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1848
    :cond_0
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    const/4 v1, 0x0

    aget v6, v0, v1

    .line 1849
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    const/4 v1, 0x1

    aget v7, v0, v1

    .line 1851
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mFlingTarget:[I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrX()I

    move-result v2

    aput v2, v0, v1

    .line 1852
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mFlingTarget:[I

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrY()I

    move-result v2

    aput v2, v0, v1

    .line 1855
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_3

    .line 1856
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mFlingTarget:[I

    aget v1, v1, v0

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    aget v2, v2, v0

    if-ge v1, v2, :cond_2

    .line 1857
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mFlingTarget:[I

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    aget v2, v2, v0

    aput v2, v1, v0

    .line 1855
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1858
    :cond_2
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mFlingTarget:[I

    aget v1, v1, v0

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v2, v2, v0

    if-le v1, v2, :cond_1

    .line 1859
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mFlingTarget:[I

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v2, v2, v0

    aput v2, v1, v0

    goto :goto_1

    .line 1865
    :cond_3
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mFlingFinal:[I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getFinalX()I

    move-result v2

    aput v2, v0, v1

    .line 1866
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mFlingFinal:[I

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getFinalY()I

    move-result v2

    aput v2, v0, v1

    .line 1870
    const/4 v0, 0x0

    :goto_2
    const/4 v1, 0x2

    if-ge v0, v1, :cond_6

    .line 1871
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mFlingFinal:[I

    aget v1, v1, v0

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    aget v2, v2, v0

    if-ge v1, v2, :cond_5

    .line 1872
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mFlingFinal:[I

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    aget v2, v2, v0

    aput v2, v1, v0

    .line 1870
    :cond_4
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1873
    :cond_5
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mFlingFinal:[I

    aget v1, v1, v0

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v2, v2, v0

    if-le v1, v2, :cond_4

    .line 1874
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mFlingFinal:[I

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v2, v2, v0

    aput v2, v1, v0

    goto :goto_3

    .line 1880
    :cond_6
    const/4 v0, 0x0

    .line 1881
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v8

    .line 1882
    const/4 v1, 0x0

    move v10, v1

    move v11, v0

    :goto_4
    const/4 v0, 0x2

    if-ge v10, v0, :cond_10

    .line 1883
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mFlingBounceStartMs:[J

    aget-wide v0, v0, v10

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_8

    .line 1886
    const-string v0, "Browser"

    const-string v1, "BOUNCE ANIMATION DISABLED shouldn\'t be here!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1889
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v0, v0, v10

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mBounceTargetPos:[I

    aget v1, v1, v10

    if-eq v0, v1, :cond_7

    .line 1890
    iget-object v12, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    sget v0, Lcom/android/browser/WebGLAnimator;->FLING_BOUNCE_ANIMATION_MS:I

    int-to-long v0, v0

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mFlingBounceStartMs:[J

    aget-wide v2, v2, v10

    sub-long v2, v8, v2

    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mBounceStartPos:[I

    aget v4, v4, v10

    iget-object v5, p0, Lcom/android/browser/WebGLAnimator;->mBounceTargetPos:[I

    aget v5, v5, v10

    invoke-static/range {v0 .. v5}, Lcom/android/browser/Kinetic;->getPosDecel(JJII)I

    move-result v0

    aput v0, v12, v10

    .line 1893
    :cond_7
    add-int/lit8 v0, v11, 0x1

    .line 1882
    :goto_5
    add-int/lit8 v1, v10, 0x1

    move v10, v1

    move v11, v0

    goto :goto_4

    .line 1894
    :cond_8
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v0, v0, v10

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mFlingFinal:[I

    aget v1, v1, v10

    if-ne v0, v1, :cond_9

    .line 1896
    add-int/lit8 v0, v11, 0x1

    goto :goto_5

    .line 1898
    :cond_9
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mFlingTarget:[I

    aget v0, v0, v10

    if-ltz v0, :cond_a

    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mFlingTarget:[I

    aget v0, v0, v10

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v1, v1, v10

    if-gt v0, v1, :cond_a

    .line 1899
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mFlingTarget:[I

    aget v1, v1, v10

    aput v1, v0, v10

    move v0, v11

    goto :goto_5

    .line 1903
    :cond_a
    const-string v0, "Browser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BOUNCE ANIMATION DISABLED shouldn\'t be here!!! mFlingTarget["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mFlingTarget:[I

    aget v2, v2, v10

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mScrollMin = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    aget v2, v2, v10

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mScrollMax = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v2, v2, v10

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1907
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mFlingOOBStartMs:[J

    aget-wide v0, v0, v10

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_b

    .line 1908
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mFlingOOBStartMs:[J

    aput-wide v8, v0, v10

    .line 1910
    :cond_b
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mFlingOOBStartMs:[J

    aget-wide v0, v0, v10

    sub-long v0, v8, v0

    .line 1911
    long-to-double v2, v0

    sget v4, Lcom/android/browser/WebGLAnimator;->FLING_EXTRA_ANIMATION_MS:I

    int-to-double v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    double-to-float v2, v2

    .line 1912
    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mFlingExtra:[I

    aget v3, v3, v10

    int-to-float v3, v3

    mul-float/2addr v2, v3

    .line 1914
    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mFlingTarget:[I

    aget v3, v3, v10

    if-gez v3, :cond_e

    .line 1915
    float-to-int v2, v2

    neg-int v2, v2

    .line 1916
    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mFlingTarget:[I

    aget v3, v3, v10

    if-ge v2, v3, :cond_c

    .line 1917
    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mFlingTarget:[I

    aget v2, v2, v10

    .line 1925
    :cond_c
    :goto_6
    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aput v2, v3, v10

    .line 1927
    sget v2, Lcom/android/browser/WebGLAnimator;->FLING_EXTRA_ANIMATION_MS:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_16

    .line 1928
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mFlingBounceStartMs:[J

    aput-wide v8, v0, v10

    .line 1929
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v0, v0, v10

    if-gez v0, :cond_f

    .line 1930
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mBounceStartPos:[I

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v1, v1, v10

    aput v1, v0, v10

    .line 1931
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mBounceTargetPos:[I

    const/4 v1, 0x0

    aput v1, v0, v10

    .line 1936
    :cond_d
    :goto_7
    add-int/lit8 v0, v11, 0x1

    goto/16 :goto_5

    .line 1920
    :cond_e
    float-to-int v2, v2

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v3, v3, v10

    add-int/2addr v2, v3

    .line 1921
    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mFlingTarget:[I

    aget v3, v3, v10

    if-le v2, v3, :cond_c

    .line 1922
    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mFlingTarget:[I

    aget v2, v2, v10

    goto :goto_6

    .line 1932
    :cond_f
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v0, v0, v10

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v1, v1, v10

    if-le v0, v1, :cond_d

    .line 1933
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mBounceStartPos:[I

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v1, v1, v10

    aput v1, v0, v10

    .line 1934
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mBounceTargetPos:[I

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v1, v1, v10

    aput v1, v0, v10

    goto :goto_7

    .line 1942
    :cond_10
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->constraintScaleForScrollAndFling()V

    .line 1944
    sget-boolean v0, Lcom/android/browser/WebGLAnimator;->DBG:Z

    if-eqz v0, :cond_11

    .line 1945
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "animateFlingLocked["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]: oldX="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", oldY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mScrollPos=("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", finalX="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mFlingFinal:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", finalY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mFlingFinal:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 1950
    :cond_11
    const/4 v0, 0x2

    if-lt v11, v0, :cond_15

    .line 1952
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 1954
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->needBounce()Z

    move-result v0

    if-nez v0, :cond_14

    .line 1957
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    if-lez v0, :cond_13

    .line 1958
    const/4 v0, 0x0

    :goto_8
    const/4 v1, 0x2

    if-ge v0, v1, :cond_13

    .line 1959
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mFlingEdgeGlowStartMs:[J

    aget-wide v1, v1, v0

    sub-long v1, v8, v1

    .line 1960
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "elapsedEdgeGlow[%d]="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 1961
    sget v3, Lcom/android/browser/WebGLAnimator;->FLING_EXTRA_ANIMATION_MS:I

    mul-int/lit8 v3, v3, 0x2

    int-to-long v3, v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_12

    .line 1962
    const/4 v0, 0x1

    .line 1974
    :goto_9
    return v0

    .line 1958
    :cond_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 1967
    :cond_13
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->requestStateChange(I)V

    .line 1971
    :goto_a
    const/4 v0, 0x0

    goto :goto_9

    .line 1969
    :cond_14
    const/16 v0, 0x40

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->requestStateChange(I)V

    goto :goto_a

    .line 1974
    :cond_15
    const/4 v0, 0x1

    goto :goto_9

    :cond_16
    move v0, v11

    goto/16 :goto_5
.end method

.method private declared-synchronized animateLocked()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1529
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollFrames:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollFrames:J

    .line 1531
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mState:I

    sparse-switch v0, :sswitch_data_0

    .line 1543
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->unimplemented()V

    .line 1544
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->requestStateChange(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v4

    .line 1545
    :goto_0
    monitor-exit p0

    return v0

    .line 1533
    :sswitch_0
    :try_start_1
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->animateScrollLocked()Z

    move-result v0

    goto :goto_0

    .line 1535
    :sswitch_1
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->animateBounceLocked()Z

    move-result v0

    goto :goto_0

    .line 1537
    :sswitch_2
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->animateZoomLocked()Z

    move-result v0

    goto :goto_0

    .line 1539
    :sswitch_3
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->animatePinchLocked()Z

    move-result v0

    goto :goto_0

    .line 1541
    :sswitch_4
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->animateFlingLocked()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 1529
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1531
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x8 -> :sswitch_4
        0x10 -> :sswitch_3
        0x20 -> :sswitch_2
        0x40 -> :sswitch_1
    .end sparse-switch
.end method

.method private animatePinchLocked()Z
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/high16 v5, 0x4480

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2022
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScaleDetector:Lcom/android/browser/DirectScaleGestureDetector;

    .line 2023
    invoke-virtual {v0}, Lcom/android/browser/DirectScaleGestureDetector;->update()V

    .line 2025
    iget-wide v1, p0, Lcom/android/browser/WebGLAnimator;->mPinchFrames:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/android/browser/WebGLAnimator;->mPinchFrames:J

    .line 2026
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mPinchMotionFilter:Lcom/android/browser/MotionFilter;

    iget v2, p0, Lcom/android/browser/WebGLAnimator;->mTargetZoomScale:F

    mul-float/2addr v2, v5

    invoke-virtual {v1, v2}, Lcom/android/browser/MotionFilter;->filter(F)F

    move-result v1

    div-float/2addr v1, v5

    iput v1, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    .line 2030
    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    iget v2, p0, Lcom/android/browser/WebGLAnimator;->mMinScale:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    .line 2031
    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mMinScale:F

    iput v1, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    .line 2036
    :cond_0
    :goto_0
    new-array v1, v9, [F

    .line 2037
    iget v2, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterX:F

    aput v2, v1, v7

    .line 2038
    iget v2, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterY:F

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    invoke-virtual {v3}, Lcom/android/browser/BitmapWebView;->getTitleBarHeight()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    aput v2, v1, v8

    .line 2040
    iget v2, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v3}, Landroid/webkit/WebView;->getScale()F

    move-result v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v3}, Landroid/webkit/WebView;->getContentWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 2041
    iget v3, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v4}, Landroid/webkit/WebView;->getScale()F

    move-result v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v4}, Landroid/webkit/WebView;->getContentHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 2043
    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollMax:[I

    iget-object v5, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->getWidth()I

    move-result v5

    sub-int/2addr v2, v5

    invoke-static {v7, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    aput v2, v4, v7

    .line 2044
    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollMax:[I

    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v4}, Landroid/webkit/WebView;->getHeight()I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v4}, Landroid/webkit/WebView;->getTitleHeight()I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {v7, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    aput v3, v2, v8

    move v2, v7

    .line 2046
    :goto_1
    if-ge v2, v9, :cond_4

    .line 2047
    aget v3, v1, v2

    aget v4, v1, v2

    iget v5, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    div-float/2addr v4, v5

    sub-float/2addr v3, v4

    .line 2048
    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mNewScroll:[F

    iget-object v5, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v5, v5, v2

    int-to-float v5, v5

    add-float/2addr v5, v3

    aput v5, v4, v2

    .line 2049
    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollInt:[I

    iget-object v5, p0, Lcom/android/browser/WebGLAnimator;->mNewScroll:[F

    aget v5, v5, v2

    iget v6, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    mul-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    aput v5, v4, v2

    .line 2050
    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    neg-float v5, v3

    float-to-int v5, v5

    aput v5, v4, v2

    .line 2051
    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    iget-object v5, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollMax:[I

    aget v5, v5, v2

    int-to-float v5, v5

    iget v6, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    div-float/2addr v5, v6

    sub-float v3, v5, v3

    float-to-int v3, v3

    aput v3, v4, v2

    .line 2052
    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollInt:[I

    aget v3, v3, v2

    if-gez v3, :cond_3

    .line 2053
    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v4, v3, v2

    iget-object v5, p0, Lcom/android/browser/WebGLAnimator;->mNewScroll:[F

    aget v5, v5, v2

    float-to-int v5, v5

    sub-int/2addr v4, v5

    aput v4, v3, v2

    .line 2054
    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v4, v4, v2

    aput v4, v3, v2

    .line 2046
    :cond_1
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2032
    :cond_2
    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    iget v2, p0, Lcom/android/browser/WebGLAnimator;->mMaxScale:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 2033
    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mMaxScale:F

    iput v1, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    goto/16 :goto_0

    .line 2055
    :cond_3
    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollInt:[I

    aget v3, v3, v2

    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollMax:[I

    aget v4, v4, v2

    if-le v3, v4, :cond_1

    .line 2056
    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollInt:[I

    aget v3, v3, v2

    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mNewScrollMax:[I

    aget v4, v4, v2

    sub-int/2addr v3, v4

    int-to-float v3, v3

    .line 2057
    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v5, v4, v2

    iget v6, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    div-float/2addr v3, v6

    float-to-int v3, v3

    sub-int v3, v5, v3

    aput v3, v4, v2

    .line 2058
    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v4, v4, v2

    aput v4, v3, v2

    goto :goto_2

    .line 2064
    :cond_4
    sget-boolean v1, Lcom/android/browser/WebGLAnimator;->DBG:Z

    if-eqz v1, :cond_6

    .line 2065
    const-string v1, ""

    .line 2066
    iget-boolean v2, p0, Lcom/android/browser/WebGLAnimator;->mPrintFirstPinch:Z

    if-eqz v2, :cond_5

    iget v2, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    const/high16 v3, 0x3f80

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_5

    .line 2067
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " started "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/android/browser/WebGLAnimator;->mSecondPointerDownMs:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms @ frame "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/browser/WebGLAnimator;->mPinchFrames:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2068
    iput-boolean v7, p0, Lcom/android/browser/WebGLAnimator;->mPrintFirstPinch:Z

    .line 2071
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SCALE: Prog = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/browser/DirectScaleGestureDetector;->isInProgress()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Span="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/browser/DirectScaleGestureDetector;->getCurrentSpan()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", x="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/browser/DirectScaleGestureDetector;->getFocusX()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", y="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/browser/DirectScaleGestureDetector;->getFocusY()F

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mTargetZoomScale = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/browser/WebGLAnimator;->mTargetZoomScale:F

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mZoomScale = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 2076
    :cond_6
    return v8
.end method

.method private animateScrollLocked()Z
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/high16 v9, 0x3fc0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1551
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mPointerPos:[I

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->getOneFingerXY([I)Z

    move-result v0

    .line 1554
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mFilteredPointerPos:[I

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mPointerPos:[I

    aget v2, v2, v7

    aput v2, v1, v7

    .line 1555
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mFilteredPointerPos:[I

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mPointerPos:[I

    aget v2, v2, v8

    aput v2, v1, v8

    .line 1556
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mMotionFilter:Lcom/android/browser/MotionFilter;

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mFilteredPointerPos:[I

    invoke-virtual {v1, v2}, Lcom/android/browser/MotionFilter;->filter([I)V

    .line 1558
    iget-boolean v1, p0, Lcom/android/browser/WebGLAnimator;->mStationary:Z

    if-eqz v1, :cond_0

    .line 1559
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mStationaryPointerPos:[I

    aget v1, v1, v7

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mPointerPos:[I

    aget v2, v2, v7

    sub-int/2addr v1, v2

    .line 1560
    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mStationaryPointerPos:[I

    aget v2, v2, v8

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mPointerPos:[I

    aget v3, v3, v8

    sub-int/2addr v2, v3

    .line 1561
    mul-int v3, v1, v1

    mul-int v4, v2, v2

    add-int/2addr v3, v4

    const/16 v4, 0x40

    if-le v3, v4, :cond_0

    .line 1562
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 1563
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v4

    .line 1564
    int-to-float v5, v3

    int-to-float v6, v4

    mul-float/2addr v6, v9

    cmpl-float v5, v5, v6

    if-lez v5, :cond_5

    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->isWebViewFixedWidth()Z

    move-result v5

    if-nez v5, :cond_5

    .line 1565
    iput v10, p0, Lcom/android/browser/WebGLAnimator;->mSnapScrollMode:I

    .line 1566
    if-lez v1, :cond_4

    move v3, v8

    :goto_0
    iput-boolean v3, p0, Lcom/android/browser/WebGLAnimator;->mSnapPositive:Z

    .line 1574
    :goto_1
    iput-boolean v7, p0, Lcom/android/browser/WebGLAnimator;->mStationary:Z

    .line 1575
    sget-boolean v3, Lcom/android/browser/WebGLAnimator;->DBG:Z

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "disabled mStationary: deltaX = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", deltaY = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", snap = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/browser/WebGLAnimator;->mSnapScrollMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 1579
    :cond_0
    iget-boolean v1, p0, Lcom/android/browser/WebGLAnimator;->mStationary:Z

    if-nez v1, :cond_1

    iget-wide v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollFrames:J

    const-wide/16 v3, 0x1

    cmp-long v1, v1, v3

    if-lez v1, :cond_1

    .line 1580
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->adjustSnapScrollMode()V

    .line 1582
    :cond_1
    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mSnapScrollMode:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_8

    .line 1583
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mTouchDown:[I

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mFilteredPointerPos:[I

    aget v2, v2, v8

    aput v2, v1, v8

    :cond_2
    :goto_2
    move v1, v7

    .line 1589
    :goto_3
    if-ge v1, v10, :cond_a

    .line 1590
    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollStart:[I

    aget v3, v3, v1

    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mTouchDown:[I

    aget v4, v4, v1

    iget-object v5, p0, Lcom/android/browser/WebGLAnimator;->mFilteredPointerPos:[I

    aget v5, v5, v1

    sub-int/2addr v4, v5

    int-to-float v4, v4

    iget v5, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    div-float/2addr v4, v5

    float-to-int v4, v4

    add-int/2addr v3, v4

    aput v3, v2, v1

    .line 1591
    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v2, v2, v1

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    aget v3, v3, v1

    if-ge v2, v3, :cond_9

    .line 1594
    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    aget v3, v3, v1

    aput v3, v2, v1

    .line 1589
    :cond_3
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_4
    move v3, v7

    .line 1566
    goto/16 :goto_0

    .line 1567
    :cond_5
    int-to-float v4, v4

    int-to-float v3, v3

    mul-float/2addr v3, v9

    cmpl-float v3, v4, v3

    if-lez v3, :cond_7

    .line 1568
    const/4 v3, 0x4

    iput v3, p0, Lcom/android/browser/WebGLAnimator;->mSnapScrollMode:I

    .line 1569
    if-lez v2, :cond_6

    move v3, v8

    :goto_5
    iput-boolean v3, p0, Lcom/android/browser/WebGLAnimator;->mSnapPositive:Z

    goto/16 :goto_1

    :cond_6
    move v3, v7

    goto :goto_5

    .line 1571
    :cond_7
    iput v7, p0, Lcom/android/browser/WebGLAnimator;->mSnapScrollMode:I

    goto/16 :goto_1

    .line 1584
    :cond_8
    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mSnapScrollMode:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 1585
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mTouchDown:[I

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mFilteredPointerPos:[I

    aget v2, v2, v7

    aput v2, v1, v7

    goto :goto_2

    .line 1599
    :cond_9
    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v2, v2, v1

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v3, v3, v1

    if-le v2, v3, :cond_3

    .line 1602
    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v3, v3, v1

    aput v3, v2, v1

    goto :goto_4

    .line 1611
    :cond_a
    :try_start_0
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/android/browser/WebGLAnimator;->mTouchDownMs:J

    sub-long/2addr v1, v3

    const-wide/16 v3, 0x64

    cmp-long v1, v1, v3

    if-ltz v1, :cond_b

    if-eqz v0, :cond_c

    .line 1612
    :cond_b
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mPointerPos:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mPointerPos:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-direct {p0, v1, v2, v0}, Lcom/android/browser/WebGLAnimator;->getMotionEventFromXY(IIZ)Landroid/view/MotionEvent;

    move-result-object v1

    .line 1613
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lcom/android/browser/WebGLAnimator;->trackVelocity(Landroid/view/MotionEvent;ZZ)V

    .line 1614
    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1621
    :cond_c
    :goto_6
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v1, v1, v7

    if-gtz v1, :cond_d

    .line 1622
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aput v7, v1, v7

    .line 1625
    :cond_d
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mLastPointerPos:[I

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mPointerPos:[I

    aget v2, v2, v7

    aput v2, v1, v7

    .line 1626
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mLastPointerPos:[I

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mPointerPos:[I

    aget v2, v2, v8

    aput v2, v1, v8

    .line 1628
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->constraintScaleForScrollAndFling()V

    .line 1630
    sget-boolean v1, Lcom/android/browser/WebGLAnimator;->DBG:Z

    if-eqz v1, :cond_e

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "animateScrollLocked: mScrollPos=("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v2, v2, v7

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v2, v2, v8

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "), dx = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v2, v2, v7

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollStart:[I

    aget v3, v3, v7

    sub-int/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", dy = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v2, v2, v8

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollStart:[I

    aget v3, v3, v8

    sub-int/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 1633
    :cond_e
    if-nez v0, :cond_f

    .line 1640
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->needFling()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1641
    const/16 v0, 0x8

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mState:I

    .line 1642
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    invoke-virtual {v0}, Lcom/android/browser/BitmapWebView;->requestRender()V

    .line 1643
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->resumeUpdateTextureBitmap()V

    .line 1644
    const-string v0, "changeStateTo (SCROLL) => FLING  *** by GL THREAD ***"

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->logIfDBG(Ljava/lang/String;)V

    .line 1660
    :cond_f
    return v8

    .line 1616
    :catch_0
    move-exception v1

    goto/16 :goto_6
.end method

.method private animateZoomLocked()Z
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/high16 v4, 0x3f80

    .line 2002
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/browser/WebGLAnimator;->mDTZoomStartMs:J

    sub-long/2addr v0, v2

    long-to-int v11, v0

    .line 2004
    .local v11, elapsed:I
    int-to-long v0, v11

    iget-wide v2, p0, Lcom/android/browser/WebGLAnimator;->mDTZoomDuration:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 2005
    iget-wide v0, p0, Lcom/android/browser/WebGLAnimator;->mDTZoomDuration:J

    int-to-long v2, v11

    iget v5, p0, Lcom/android/browser/WebGLAnimator;->mDTTargetZoomScale:F

    invoke-static/range {v0 .. v5}, Lcom/android/browser/Kinetic;->getScaleDecel(JJFF)F

    move-result v0

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    .line 2018
    :goto_0
    return v12

    .line 2006
    :cond_0
    iget-boolean v0, p0, Lcom/android/browser/WebGLAnimator;->mDTBounceBack:Z

    if-eqz v0, :cond_1

    int-to-long v0, v11

    const-wide/16 v2, 0x2

    iget-wide v5, p0, Lcom/android/browser/WebGLAnimator;->mDTZoomDuration:J

    mul-long/2addr v2, v5

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 2007
    int-to-long v0, v11

    iget-wide v2, p0, Lcom/android/browser/WebGLAnimator;->mDTZoomDuration:J

    sub-long/2addr v0, v2

    long-to-int v11, v0

    .line 2008
    iget-wide v5, p0, Lcom/android/browser/WebGLAnimator;->mDTZoomDuration:J

    int-to-long v7, v11

    iget v9, p0, Lcom/android/browser/WebGLAnimator;->mDTTargetZoomScale:F

    move v10, v4

    invoke-static/range {v5 .. v10}, Lcom/android/browser/Kinetic;->getScaleDecel(JJFF)F

    move-result v0

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    goto :goto_0

    .line 2010
    :cond_1
    iget-boolean v0, p0, Lcom/android/browser/WebGLAnimator;->mDTBounceBack:Z

    if-eqz v0, :cond_2

    .line 2011
    iput v4, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    .line 2015
    :goto_1
    invoke-direct {p0, v12}, Lcom/android/browser/WebGLAnimator;->requestStateChange(I)V

    goto :goto_0

    .line 2013
    :cond_2
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mDTTargetZoomScale:F

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    goto :goto_1
.end method

.method private declared-synchronized changeStateTo(I)V
    .locals 9
    .parameter "newState"

    .prologue
    const/4 v8, 0x1

    const-string v0, "Scroll"

    .line 605
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/browser/WebGLAnimator;->assertCalledOnUIThread()V

    .line 607
    iget v6, p0, Lcom/android/browser/WebGLAnimator;->mState:I

    .line 608
    .local v6, oldState:I
    iput p1, p0, Lcom/android/browser/WebGLAnimator;->mState:I

    .line 610
    if-eq v6, p1, :cond_0

    .line 611
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mGlThreadID:I

    if-ltz v0, :cond_0

    .line 613
    if-ne p1, v8, :cond_4

    .line 614
    const/4 v7, 0x0

    .line 619
    .local v7, prio:I
    :goto_0
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mGlThreadID:I

    invoke-static {v0, v7}, Landroid/os/Process;->setThreadPriority(II)V

    .line 623
    .end local v7           #prio:I
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "changeStateTo ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, v6}, Lcom/android/browser/WebGLAnimator;->getStateName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") => "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/android/browser/WebGLAnimator;->getStateName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->logIfDBG(Ljava/lang/String;)V

    .line 626
    shl-int/lit8 v0, v6, 0x7

    or-int/2addr v0, p1

    sparse-switch v0, :sswitch_data_0

    .line 727
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->unimplemented()V

    .line 730
    :goto_1
    :sswitch_0
    if-ne p1, v8, :cond_3

    .line 731
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_1

    .line 732
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 733
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 735
    :cond_1
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTrackerQuickFlick:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_2

    .line 736
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTrackerQuickFlick:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 737
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTrackerQuickFlick:Landroid/view/VelocityTracker;

    .line 739
    :cond_2
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/browser/BitmapWebView;->setIsInProgress(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 741
    :cond_3
    monitor-exit p0

    return-void

    .line 616
    :cond_4
    const/4 v7, -0x4

    .restart local v7       #prio:I
    goto :goto_0

    .line 628
    .end local v7           #prio:I
    :sswitch_1
    :try_start_1
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->startPinchFromScrollState()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 605
    .end local v6           #oldState:I
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 632
    .restart local v6       #oldState:I
    :sswitch_2
    :try_start_2
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/browser/WebGLAnimator;->mTouchDownMs:J

    .line 633
    const/high16 v0, 0x3f80

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mTargetZoomScale:F

    .line 634
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScaleDetector:Lcom/android/browser/DirectScaleGestureDetector;

    invoke-virtual {v0}, Lcom/android/browser/DirectScaleGestureDetector;->reset()V

    .line 635
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/browser/WebGLAnimator;->mIsPinchCenterInited:Z

    .line 636
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScaleDetector:Lcom/android/browser/DirectScaleGestureDetector;

    invoke-virtual {v0}, Lcom/android/browser/DirectScaleGestureDetector;->update()V

    .line 637
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mTouchDown:[I

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->getOneFingerXY([I)Z

    .line 638
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    invoke-virtual {v0}, Lcom/android/browser/BitmapWebView;->isMobilePage()Z

    move-result v0

    if-nez v0, :cond_5

    .line 639
    invoke-virtual {p0}, Lcom/android/browser/WebGLAnimator;->refrainUpdateTextureBitmap()V

    .line 641
    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/browser/WebGLAnimator;->mTouchDownStale:Z

    .line 642
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/browser/WebGLAnimator;->mBounceStartMs:J

    goto :goto_1

    .line 649
    :sswitch_3
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->reduceWebViewPriority()V

    .line 650
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/browser/WebGLAnimator;->mStationary:Z

    .line 651
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollStartMs:J

    .line 652
    const/high16 v0, 0x3f80

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollStartScale:F

    .line 653
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollFrames:J

    .line 654
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    invoke-virtual {v0}, Lcom/android/browser/BitmapWebView;->show()V

    .line 655
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    invoke-virtual {v0}, Lcom/android/browser/BitmapWebView;->requestRender()V

    .line 656
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    invoke-virtual {v0}, Lcom/android/browser/BitmapWebView;->isMobilePage()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 657
    const-wide/16 v0, 0xc8

    invoke-direct {p0, v0, v1}, Lcom/android/browser/WebGLAnimator;->resumeUpdateTextureBitmap(J)V

    goto/16 :goto_1

    .line 659
    :cond_6
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->resumeUpdateTextureBitmap()V

    goto/16 :goto_1

    .line 664
    :sswitch_4
    sget-boolean v0, Lcom/android/browser/WebGLAnimator;->DBG:Z

    if-eqz v0, :cond_7

    const-string v1, "Scroll"

    iget-wide v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollStartMs:J

    iget-wide v4, p0, Lcom/android/browser/WebGLAnimator;->mScrollFrames:J

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/browser/WebGLAnimator;->showFPS(Ljava/lang/String;JJ)V

    .line 665
    :cond_7
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->hideLocked()V

    goto/16 :goto_1

    .line 669
    :sswitch_5
    sget-boolean v0, Lcom/android/browser/WebGLAnimator;->DBG:Z

    if-eqz v0, :cond_8

    const-string v1, "Scroll"

    iget-wide v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollStartMs:J

    iget-wide v4, p0, Lcom/android/browser/WebGLAnimator;->mScrollFrames:J

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/browser/WebGLAnimator;->showFPS(Ljava/lang/String;JJ)V

    .line 670
    :cond_8
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->hideLocked()V

    goto/16 :goto_1

    .line 676
    :sswitch_6
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/browser/WebGLAnimator;->mStationary:Z

    .line 677
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mStationaryPointerPos:[I

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->getOneFingerXY([I)Z

    .line 678
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->obtainNewVelocityTracker()Landroid/view/VelocityTracker;

    goto/16 :goto_1

    .line 682
    :sswitch_7
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->initBounce()V

    goto/16 :goto_1

    .line 686
    :sswitch_8
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    invoke-virtual {v0}, Lcom/android/browser/BitmapWebView;->requestRender()V

    .line 687
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->resumeUpdateTextureBitmap()V

    goto/16 :goto_1

    .line 692
    :sswitch_9
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/browser/BitmapWebView;->setIsInProgress(Z)V

    .line 694
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->startPinch()V

    .line 695
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->resumeUpdateTextureBitmap()V

    goto/16 :goto_1

    .line 699
    :sswitch_a
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->hideLocked()V

    goto/16 :goto_1

    .line 703
    :sswitch_b
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->initBounce()V

    goto/16 :goto_1

    .line 707
    :sswitch_c
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->initBounce()V

    .line 708
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    invoke-virtual {v0}, Lcom/android/browser/BitmapWebView;->requestRender()V

    goto/16 :goto_1

    .line 712
    :sswitch_d
    sget-boolean v0, Lcom/android/browser/WebGLAnimator;->DBG:Z

    if-eqz v0, :cond_9

    const-string v1, "Scroll"

    iget-wide v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollStartMs:J

    iget-wide v4, p0, Lcom/android/browser/WebGLAnimator;->mScrollFrames:J

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/browser/WebGLAnimator;->showFPS(Ljava/lang/String;JJ)V

    .line 713
    :cond_9
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->hideLocked()V

    goto/16 :goto_1

    .line 717
    :sswitch_e
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    invoke-virtual {v0}, Lcom/android/browser/BitmapWebView;->show()V

    .line 718
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    invoke-virtual {v0}, Lcom/android/browser/BitmapWebView;->requestRender()V

    .line 719
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->resumeUpdateTextureBitmap()V

    goto/16 :goto_1

    .line 723
    :sswitch_f
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->hideLocked()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 626
    nop

    :sswitch_data_0
    .sparse-switch
        0x82 -> :sswitch_2
        0x90 -> :sswitch_9
        0xa0 -> :sswitch_e
        0x101 -> :sswitch_0
        0x104 -> :sswitch_3
        0x110 -> :sswitch_9
        0x201 -> :sswitch_4
        0x208 -> :sswitch_8
        0x210 -> :sswitch_1
        0x240 -> :sswitch_7
        0x401 -> :sswitch_d
        0x404 -> :sswitch_6
        0x440 -> :sswitch_c
        0x801 -> :sswitch_a
        0x804 -> :sswitch_6
        0x840 -> :sswitch_b
        0x1001 -> :sswitch_f
        0x2001 -> :sswitch_5
        0x2004 -> :sswitch_6
    .end sparse-switch
.end method

.method private checkWebViewScrollAndZoom()Z
    .locals 2

    .prologue
    .line 839
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/browser/WebGLAnimator;->_setWebViewScrollAndZoom(ZZ)Z

    move-result v0

    return v0
.end method

.method private constraintScaleForScrollAndFling()V
    .locals 3

    .prologue
    .line 1670
    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    iget v2, p0, Lcom/android/browser/WebGLAnimator;->mMaxScale:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 1671
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mMaxScale:F

    .line 1690
    .local v0, targetScale:F
    :goto_0
    return-void

    .line 1672
    .end local v0           #targetScale:F
    :cond_0
    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    iget v2, p0, Lcom/android/browser/WebGLAnimator;->mMinScale:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    .line 1673
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mMinScale:F

    .restart local v0       #targetScale:F
    goto :goto_0

    .line 1675
    .end local v0           #targetScale:F
    :cond_1
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    .restart local v0       #targetScale:F
    goto :goto_0
.end method

.method private getActionEx(Landroid/view/MotionEvent;)I
    .locals 1
    .parameter "ev"

    .prologue
    .line 1405
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1412
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1406
    :pswitch_1
    const/16 v0, 0x80

    goto :goto_0

    .line 1407
    :pswitch_2
    const/16 v0, 0x100

    goto :goto_0

    .line 1408
    :pswitch_3
    const/16 v0, 0x200

    goto :goto_0

    .line 1409
    :pswitch_4
    const/16 v0, 0x400

    goto :goto_0

    .line 1410
    :pswitch_5
    const/16 v0, 0x800

    goto :goto_0

    .line 1405
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private getConfigString()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v2, ","

    .line 2132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mScrollPos = ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v1, v1, v4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), min = ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    aget v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    aget v1, v1, v4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), max = ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v1, v1, v4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") mZoomCenter = ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterX:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterY:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), mZoomScale = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getMotionEventFromXY(IIZ)Landroid/view/MotionEvent;
    .locals 9
    .parameter "x"
    .parameter "y"
    .parameter "fingerIsDown"

    .prologue
    .line 2435
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 2436
    .local v0, downTime:J
    move-wide v2, v0

    .line 2437
    .local v2, eventTime:J
    const/4 v7, 0x0

    .line 2438
    .local v7, metaState:I
    if-eqz p3, :cond_0

    const/4 v5, 0x2

    move v4, v5

    .line 2440
    .local v4, action:I
    :goto_0
    int-to-float v5, p1

    int-to-float v6, p2

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    .line 2442
    .local v8, ev:Landroid/view/MotionEvent;
    return-object v8

    .line 2438
    .end local v4           #action:I
    .end local v8           #ev:Landroid/view/MotionEvent;
    :cond_0
    const/4 v5, 0x1

    move v4, v5

    goto :goto_0
.end method

.method private getOneFingerXY([I)Z
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2456
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mStates:[Landroid/view/PointerDevice$PointerState;

    if-nez v0, :cond_0

    .line 2457
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mPointerDevice:Landroid/view/PointerDevice;

    invoke-virtual {v0}, Landroid/view/PointerDevice;->newPointerStates()[Landroid/view/PointerDevice$PointerState;

    move-result-object v0

    iput-object v0, p0, Lcom/android/browser/WebGLAnimator;->mStates:[Landroid/view/PointerDevice$PointerState;

    .line 2459
    :cond_0
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mPointerDevice:Landroid/view/PointerDevice;

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mStates:[Landroid/view/PointerDevice$PointerState;

    invoke-virtual {v0, v1}, Landroid/view/PointerDevice;->update([Landroid/view/PointerDevice$PointerState;)V

    move v0, v3

    .line 2462
    :goto_0
    const/4 v1, 0x5

    if-ge v0, v1, :cond_4

    .line 2463
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mStates:[Landroid/view/PointerDevice$PointerState;

    aget-object v1, v1, v0

    iget-boolean v1, v1, Landroid/view/PointerDevice$PointerState;->pressed:Z

    if-ne v1, v4, :cond_3

    .line 2464
    sget-boolean v1, Lcom/android/browser/WebGLAnimator;->DBG:Z

    if-eqz v1, :cond_1

    .line 2465
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getOneFingerXY id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mStates:[Landroid/view/PointerDevice$PointerState;

    aget-object v2, v2, v0

    iget v2, v2, Landroid/view/PointerDevice$PointerState;->id:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", x = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mStates:[Landroid/view/PointerDevice$PointerState;

    aget-object v2, v2, v0

    iget v2, v2, Landroid/view/PointerDevice$PointerState;->x:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", y = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mStates:[Landroid/view/PointerDevice$PointerState;

    aget-object v2, v2, v0

    iget v2, v2, Landroid/view/PointerDevice$PointerState;->y:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 2468
    :cond_1
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mStates:[Landroid/view/PointerDevice$PointerState;

    aget-object v1, v1, v0

    iget v1, v1, Landroid/view/PointerDevice$PointerState;->x:I

    aput v1, p1, v3

    .line 2469
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mStates:[Landroid/view/PointerDevice$PointerState;

    aget-object v0, v1, v0

    iget v0, v0, Landroid/view/PointerDevice$PointerState;->y:I

    aput v0, p1, v4

    move v0, v4

    .line 2475
    :goto_1
    if-nez v0, :cond_2

    .line 2476
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mPointerDevice:Landroid/view/PointerDevice;

    invoke-virtual {v1, p1}, Landroid/view/PointerDevice;->getXY([I)V

    .line 2477
    sget-boolean v1, Lcom/android/browser/WebGLAnimator;->DBG:Z

    if-eqz v1, :cond_2

    .line 2478
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getOneFingerXY: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget v2, p1, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget v2, p1, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 2482
    :cond_2
    return v0

    .line 2462
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_4
    move v0, v3

    goto :goto_1
.end method

.method private getStateName(I)Ljava/lang/String;
    .locals 3
    .parameter "state"

    .prologue
    .line 160
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->stateNames:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 161
    const/4 v1, 0x1

    shl-int/2addr v1, v0

    and-int/2addr v1, p1

    if-eqz v1, :cond_0

    .line 162
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->stateNames:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 165
    :goto_1
    return-object v1

    .line 160
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 165
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private hideLocked()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/high16 v5, 0x3f80

    .line 775
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/browser/WebGLAnimator;->mHideLockedStartMs:J

    .line 778
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    invoke-virtual {v1}, Lcom/android/browser/BitmapWebView;->requestRender()V

    .line 783
    invoke-virtual {p0}, Lcom/android/browser/WebGLAnimator;->refrainUpdateTextureBitmap()V

    .line 786
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->pauseGLLocked()V

    .line 787
    iput-boolean v6, p0, Lcom/android/browser/WebGLAnimator;->mDrawingLastFrame:Z

    .line 790
    const-wide/16 v1, 0x5

    invoke-static {v1, v2}, Landroid/os/SystemClock;->sleep(J)V

    .line 793
    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    cmpl-float v1, v1, v5

    if-nez v1, :cond_1

    .line 794
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v1

    sget v3, Lcom/android/browser/WebGLAnimator;->MAX_HIDE_LOCKSCREEN_SAME_SIZE_MS:I

    int-to-long v3, v3

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/android/browser/WebGLAnimator;->mGLWakeupTime:J

    .line 795
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v1

    sget v3, Lcom/android/browser/WebGLAnimator;->MIN_HIDE_LOCKSCREEN_SAME_SIZE_MS:I

    int-to-long v3, v3

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/android/browser/WebGLAnimator;->mGLWakeupTimeMin:J

    .line 800
    :goto_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 802
    const-string v1, "mZoomView.hide()"

    invoke-direct {p0, v1}, Lcom/android/browser/WebGLAnimator;->logIfDBG(Ljava/lang/String;)V

    .line 806
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    invoke-virtual {v1}, Lcom/android/browser/BitmapWebView;->hide()V

    .line 808
    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    cmpl-float v1, v1, v5

    if-eqz v1, :cond_2

    move v0, v6

    .line 809
    .local v0, updatePlugins:Z
    :goto_1
    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->setWebViewScrollAndZoom(Z)V

    .line 810
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->resumeWebViewPriority()V

    .line 811
    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    float-to-double v1, v1

    const-wide/high16 v3, 0x3ff0

    cmpl-double v1, v1, v3

    if-eqz v1, :cond_0

    .line 812
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    invoke-virtual {v1}, Lcom/android/browser/BitmapWebView;->setGrabFromNextCanvasPost()V

    .line 814
    :cond_0
    return-void

    .line 797
    .end local v0           #updatePlugins:Z
    :cond_1
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v1

    sget v3, Lcom/android/browser/WebGLAnimator;->MAX_HIDE_LOCKSCREEN_RESIZE_MS:I

    int-to-long v3, v3

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/android/browser/WebGLAnimator;->mGLWakeupTime:J

    .line 798
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v1

    sget v3, Lcom/android/browser/WebGLAnimator;->MIN_HIDE_LOCKSCREEN_RESIZE_MS:I

    int-to-long v3, v3

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/android/browser/WebGLAnimator;->mGLWakeupTimeMin:J

    goto :goto_0

    .line 808
    :cond_2
    const/4 v1, 0x0

    move v0, v1

    goto :goto_1
.end method

.method private initBounce()V
    .locals 6

    .prologue
    const/high16 v2, 0x3f80

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v5, ", "

    .line 942
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/browser/WebGLAnimator;->mBounceStartMs:J

    .line 945
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_0

    .line 950
    :cond_0
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mBounceStartZoomScale:F

    .line 951
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mBounceTargetZoomScale:F

    .line 952
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mBounceTargetZoomScale:F

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mMaxScale:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mBounceTargetZoomScale:F

    .line 953
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mBounceTargetZoomScale:F

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mMinScale:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mBounceTargetZoomScale:F

    .line 955
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mBounceStartZoomScale:F

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mBounceTargetZoomScale:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_2

    .line 958
    :cond_1
    sget v0, Lcom/android/browser/WebGLAnimator;->QUICK_BOUNCE_ANIMATION_MS:I

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/android/browser/WebGLAnimator;->mBouncePeriodMs:J

    :goto_0
    move v0, v3

    .line 963
    :goto_1
    const/4 v1, 0x2

    if-ge v0, v1, :cond_5

    .line 964
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mBounceStartPos:[I

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v2, v2, v0

    aput v2, v1, v0

    .line 965
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v1, v1, v0

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    aget v2, v2, v0

    if-ge v1, v2, :cond_3

    .line 966
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mBounceTargetPos:[I

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    aget v2, v2, v0

    aput v2, v1, v0

    .line 963
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 960
    :cond_2
    sget v0, Lcom/android/browser/WebGLAnimator;->BOUNCE_ANIMATION_MS:I

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/android/browser/WebGLAnimator;->mBouncePeriodMs:J

    goto :goto_0

    .line 967
    :cond_3
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v1, v1, v0

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v2, v2, v0

    if-le v1, v2, :cond_4

    .line 968
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mBounceTargetPos:[I

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v2, v2, v0

    aput v2, v1, v0

    goto :goto_2

    .line 970
    :cond_4
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mBounceTargetPos:[I

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v2, v2, v0

    aput v2, v1, v0

    goto :goto_2

    .line 974
    :cond_5
    sget-boolean v0, Lcom/android/browser/WebGLAnimator;->DBG:Z

    if-eqz v0, :cond_6

    .line 975
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BOUNCE: period  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/browser/WebGLAnimator;->mBouncePeriodMs:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 976
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BOUNCE: min  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    aget v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    aget v1, v1, v4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 977
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BOUNCE: max  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v1, v1, v4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 978
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BOUNCE: target  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mBounceTargetPos:[I

    aget v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mBounceTargetPos:[I

    aget v1, v1, v4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 980
    :cond_6
    return-void
.end method

.method private isState(I)Z
    .locals 1
    .parameter "mask"

    .prologue
    .line 300
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mState:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isWebViewFixedWidth()Z
    .locals 2

    .prologue
    .line 504
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getMinZoomScale()F

    move-result v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getMaxZoomScale()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 505
    const/4 v0, 0x0

    .line 507
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .parameter "s"

    .prologue
    .line 150
    const-string v0, "WebGLAnimator"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    return-void
.end method

.method private logIfDBG(Ljava/lang/String;)V
    .locals 1
    .parameter "s"

    .prologue
    .line 154
    sget-boolean v0, Lcom/android/browser/WebGLAnimator;->DBG:Z

    if-eqz v0, :cond_0

    .line 155
    const-string v0, "WebGLAnimator"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    :cond_0
    return-void
.end method

.method private needBounce()Z
    .locals 1

    .prologue
    .line 826
    const/4 v0, 0x0

    return v0
.end method

.method private declared-synchronized needFling()Z
    .locals 15

    .prologue
    const/4 v11, 0x2

    const/4 v7, 0x0

    const/4 v14, 0x1

    const/4 v13, 0x0

    .line 1694
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTrackerQuickFlick:Landroid/view/VelocityTracker;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    move v0, v13

    .line 1824
    :goto_0
    monitor-exit p0

    return v0

    .line 1698
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    const/4 v1, 0x0

    aget v6, v0, v1

    .line 1699
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    const/4 v1, 0x1

    aget v8, v0, v1

    .line 1700
    const v0, 0x3f99999a

    .line 1702
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v2, 0x3e8

    iget v3, p0, Lcom/android/browser/WebGLAnimator;->mMaximumFling:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 1703
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTrackerQuickFlick:Landroid/view/VelocityTracker;

    const/16 v2, 0x3e8

    iget v3, p0, Lcom/android/browser/WebGLAnimator;->mMaximumFling:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 1705
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v1

    mul-float/2addr v1, v0

    float-to-int v1, v1

    neg-int v1, v1

    .line 1706
    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v2

    mul-float/2addr v2, v0

    float-to-int v2, v2

    neg-int v2, v2

    .line 1707
    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTrackerQuickFlick:Landroid/view/VelocityTracker;

    invoke-virtual {v3}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v3

    mul-float/2addr v3, v0

    float-to-int v3, v3

    neg-int v3, v3

    .line 1708
    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTrackerQuickFlick:Landroid/view/VelocityTracker;

    invoke-virtual {v4}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v4

    mul-float/2addr v0, v4

    float-to-int v0, v0

    neg-int v0, v0

    .line 1710
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v4

    iget-wide v9, p0, Lcom/android/browser/WebGLAnimator;->mTouchDownMs:J

    sub-long/2addr v4, v9

    const-wide/16 v9, 0xfa

    cmp-long v4, v4, v9

    if-gez v4, :cond_13

    .line 1717
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v4

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v5

    if-ge v4, v5, :cond_2

    move v1, v3

    .line 1720
    :cond_2
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-ge v3, v4, :cond_13

    .line 1724
    :goto_1
    iget v2, p0, Lcom/android/browser/WebGLAnimator;->mSnapScrollMode:I

    if-eqz v2, :cond_3

    .line 1725
    iget v2, p0, Lcom/android/browser/WebGLAnimator;->mSnapScrollMode:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v11, :cond_6

    move v0, v13

    .line 1732
    :cond_3
    :goto_2
    if-nez v6, :cond_4

    .line 1733
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    if-ge v2, v3, :cond_4

    move v0, v13

    .line 1739
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doFling: max = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", vel = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/browser/WebGLAnimator;->logIfDBG(Ljava/lang/String;)V

    move v2, v13

    .line 1749
    :goto_3
    if-ge v2, v11, :cond_8

    .line 1750
    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mFlingBounceStartMs:[J

    const-wide/16 v4, -0x1

    aput-wide v4, v3, v2

    .line 1754
    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v3, v3, v2

    if-gez v3, :cond_7

    .line 1757
    const-string v3, "Browser"

    const-string v4, "BOUNCE ANIMATION DISABLED shouldn\'t be here!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1760
    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mBounceStartPos:[I

    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v4, v4, v2

    aput v4, v3, v2

    .line 1761
    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mBounceTargetPos:[I

    const/4 v4, 0x0

    aput v4, v3, v2

    .line 1762
    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mFlingBounceStartMs:[J

    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v4

    aput-wide v4, v3, v2

    .line 1749
    :cond_5
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_6
    move v1, v13

    .line 1728
    goto :goto_2

    .line 1763
    :cond_7
    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v3, v3, v2

    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v4, v4, v2

    if-le v3, v4, :cond_5

    .line 1766
    const-string v3, "Browser"

    const-string v4, "BOUNCE ANIMATION DISABLED shouldn\'t be here!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1769
    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mBounceStartPos:[I

    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v4, v4, v2

    aput v4, v3, v2

    .line 1770
    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mBounceTargetPos:[I

    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v4, v4, v2

    aput v4, v3, v2

    .line 1771
    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mFlingBounceStartMs:[J

    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v4

    aput-wide v4, v3, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    .line 1694
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1774
    :cond_8
    :try_start_2
    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mFlingBounceStartMs:[J

    const/4 v3, 0x0

    aget-wide v2, v2, v3

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_9

    move v1, v13

    .line 1777
    :cond_9
    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mFlingBounceStartMs:[J

    const/4 v3, 0x1

    aget-wide v2, v2, v3

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_a

    move v0, v13

    .line 1782
    :cond_a
    if-nez v6, :cond_b

    if-eqz v0, :cond_c

    :cond_b
    if-nez v8, :cond_d

    if-nez v1, :cond_d

    :cond_c
    move v0, v13

    .line 1783
    goto/16 :goto_0

    .line 1786
    :cond_d
    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrVelocity()F

    move-result v2

    .line 1787
    iget v3, p0, Lcom/android/browser/WebGLAnimator;->mLastVelocity:F

    cmpl-float v3, v3, v7

    if-lez v3, :cond_12

    cmpl-float v3, v2, v7

    if-lez v3, :cond_12

    .line 1788
    iget v3, p0, Lcom/android/browser/WebGLAnimator;->mLastVelY:F

    float-to-double v3, v3

    iget v5, p0, Lcom/android/browser/WebGLAnimator;->mLastVelX:F

    float-to-double v9, v5

    invoke-static {v3, v4, v9, v10}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v3

    int-to-double v9, v0

    int-to-double v11, v1

    invoke-static {v9, v10, v11, v12}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v9

    sub-double/2addr v3, v9

    invoke-static {v3, v4}, Ljava/lang/Math;->abs(D)D

    move-result-wide v3

    double-to-float v3, v3

    .line 1791
    const v4, 0x40b4f4ab

    cmpl-float v4, v3, v4

    if-gtz v4, :cond_e

    const v4, 0x3f20d97c

    cmpg-float v4, v3, v4

    if-gez v4, :cond_11

    .line 1792
    :cond_e
    int-to-float v1, v1

    iget v3, p0, Lcom/android/browser/WebGLAnimator;->mLastVelX:F

    mul-float/2addr v3, v2

    iget v4, p0, Lcom/android/browser/WebGLAnimator;->mLastVelocity:F

    div-float/2addr v3, v4

    add-float/2addr v1, v3

    float-to-int v1, v1

    .line 1793
    int-to-float v0, v0

    iget v3, p0, Lcom/android/browser/WebGLAnimator;->mLastVelY:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/android/browser/WebGLAnimator;->mLastVelocity:F

    div-float/2addr v2, v3

    add-float/2addr v0, v2

    float-to-int v0, v0

    .line 1794
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doFling vx= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " vy="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/browser/WebGLAnimator;->logIfDBG(Ljava/lang/String;)V

    :goto_5
    move v4, v0

    move v3, v1

    .line 1805
    :goto_6
    int-to-float v0, v3

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mLastVelX:F

    .line 1806
    int-to-float v0, v4

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mLastVelY:F

    .line 1807
    int-to-double v0, v3

    int-to-double v9, v4

    invoke-static {v0, v1, v9, v10}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mLastVelocity:F

    .line 1809
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    div-int/lit8 v0, v0, 0x8

    .line 1810
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mFlingExtra:[I

    const/4 v2, 0x0

    aput v0, v1, v2

    .line 1811
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mFlingExtra:[I

    const/4 v2, 0x1

    aput v0, v1, v2

    .line 1812
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mFlingOOBStartMs:[J

    const/4 v1, 0x0

    const-wide/16 v9, -0x1

    aput-wide v9, v0, v1

    .line 1813
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mFlingOOBStartMs:[J

    const/4 v1, 0x1

    const-wide/16 v9, -0x1

    aput-wide v9, v0, v1

    .line 1815
    if-nez v6, :cond_f

    .line 1816
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mFlingExtra:[I

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput v2, v0, v1

    .line 1818
    :cond_f
    if-nez v8, :cond_10

    .line 1819
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mFlingExtra:[I

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput v2, v0, v1

    .line 1821
    :cond_10
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScroller:Landroid/widget/Scroller;

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    const/4 v5, 0x1

    aget v2, v2, v5

    iget-object v5, p0, Lcom/android/browser/WebGLAnimator;->mFlingExtra:[I

    const/4 v7, 0x0

    aget v5, v5, v7

    sub-int v5, v13, v5

    iget-object v7, p0, Lcom/android/browser/WebGLAnimator;->mFlingExtra:[I

    const/4 v9, 0x0

    aget v7, v7, v9

    add-int/2addr v6, v7

    iget-object v7, p0, Lcom/android/browser/WebGLAnimator;->mFlingExtra:[I

    const/4 v9, 0x1

    aget v7, v7, v9

    sub-int v7, v13, v7

    iget-object v9, p0, Lcom/android/browser/WebGLAnimator;->mFlingExtra:[I

    const/4 v10, 0x1

    aget v9, v9, v10

    add-int/2addr v8, v9

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    move v0, v14

    .line 1824
    goto/16 :goto_0

    .line 1796
    :cond_11
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doFling missed "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v4, 0x40c90fdb

    div-float/2addr v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/browser/WebGLAnimator;->logIfDBG(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1799
    :cond_12
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doFling start last="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/browser/WebGLAnimator;->mLastVelocity:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " current="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " vx="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " vy="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " maxX="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " maxY="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mScrollPos=("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/browser/WebGLAnimator;->logIfDBG(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v4, v0

    move v3, v1

    goto/16 :goto_6

    :cond_13
    move v0, v2

    goto/16 :goto_1
.end method

.method private now()J
    .locals 2

    .prologue
    .line 1215
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method private obtainNewVelocityTracker()Landroid/view/VelocityTracker;
    .locals 1

    .prologue
    .line 2421
    invoke-virtual {p0}, Lcom/android/browser/WebGLAnimator;->assertCalledOnUIThread()V

    .line 2423
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 2424
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 2426
    :cond_0
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTrackerQuickFlick:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_1

    .line 2427
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTrackerQuickFlick:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 2429
    :cond_1
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 2430
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTrackerQuickFlick:Landroid/view/VelocityTracker;

    .line 2431
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTracker:Landroid/view/VelocityTracker;

    return-object v0
.end method

.method private pauseGLLocked()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 753
    iput-boolean v6, p0, Lcom/android/browser/WebGLAnimator;->mGLHasPaused:Z

    .line 754
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v2

    const-wide/16 v4, 0x2710

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/android/browser/WebGLAnimator;->mGLWakeupTime:J

    .line 755
    iput-boolean v6, p0, Lcom/android/browser/WebGLAnimator;->mWebViewDone:Z

    .line 757
    const-string v2, "pauseGLLocked enter"

    invoke-direct {p0, v2}, Lcom/android/browser/WebGLAnimator;->logIfDBG(Ljava/lang/String;)V

    .line 758
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v0

    .line 759
    .local v0, started:J
    :goto_0
    iget-boolean v2, p0, Lcom/android/browser/WebGLAnimator;->mGLHasPaused:Z

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v2

    sub-long/2addr v2, v0

    const-wide/16 v4, 0xc8

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 761
    const-wide/16 v2, 0x32

    :try_start_0
    invoke-virtual {p0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 762
    :catch_0
    move-exception v2

    goto :goto_0

    .line 764
    :cond_0
    iget-boolean v2, p0, Lcom/android/browser/WebGLAnimator;->mGLHasPaused:Z

    if-nez v2, :cond_1

    .line 765
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pauseGLLocked has failed -- GL thread is dead or stuck?? "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v3

    sub-long/2addr v3, v0

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 767
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pauseGLLocked done "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v3

    sub-long/2addr v3, v0

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ms, mGLHasPaused = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/browser/WebGLAnimator;->mGLHasPaused:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/browser/WebGLAnimator;->logIfDBG(Ljava/lang/String;)V

    .line 768
    return-void
.end method

.method private declared-synchronized requestStateChange(I)V
    .locals 3
    .parameter "reqState"

    .prologue
    .line 585
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mState:I

    .line 586
    .local v0, startState:I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestStateChange: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/browser/WebGLAnimator;->mState:I

    invoke-direct {p0, v2}, Lcom/android/browser/WebGLAnimator;->getStateName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/android/browser/WebGLAnimator;->getStateName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/browser/WebGLAnimator;->logIfDBG(Ljava/lang/String;)V

    .line 587
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    new-instance v2, Lcom/android/browser/WebGLAnimator$1;

    invoke-direct {v2, p0, v0, p1}, Lcom/android/browser/WebGLAnimator$1;-><init>(Lcom/android/browser/WebGLAnimator;II)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 601
    monitor-exit p0

    return-void

    .line 585
    .end local v0           #startState:I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private resumeUpdateTextureBitmap()V
    .locals 1

    .prologue
    .line 1288
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    invoke-virtual {v0}, Lcom/android/browser/BitmapWebView;->resumeUpdateContents()V

    .line 1289
    return-void
.end method

.method private resumeUpdateTextureBitmap(J)V
    .locals 2
    .parameter "timeout"

    .prologue
    .line 1292
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/android/browser/WebGLAnimator$2;

    invoke-direct {v1, p0}, Lcom/android/browser/WebGLAnimator$2;-><init>(Lcom/android/browser/WebGLAnimator;)V

    invoke-virtual {v0, v1, p1, p2}, Landroid/webkit/WebView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1307
    return-void
.end method

.method private setWebViewScrollAndZoom()V
    .locals 2

    .prologue
    .line 843
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/android/browser/WebGLAnimator;->_setWebViewScrollAndZoom(ZZ)Z

    .line 844
    return-void
.end method

.method private setWebViewScrollAndZoom(Z)V
    .locals 1
    .parameter "updatePlugins"

    .prologue
    .line 847
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/android/browser/WebGLAnimator;->_setWebViewScrollAndZoom(ZZ)Z

    .line 848
    return-void
.end method

.method private shouldNotReachHere(Ljava/lang/String;)V
    .locals 0
    .parameter "reason"

    .prologue
    .line 2376
    return-void
.end method

.method private showFPS(Ljava/lang/String;JJ)V
    .locals 8
    .parameter
    .parameter
    .parameter

    .prologue
    const-wide/16 v3, 0x1

    .line 1219
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v0

    sub-long/2addr v0, p2

    .line 1220
    cmp-long v2, v0, v3

    if-gez v2, :cond_0

    move-wide v0, v3

    .line 1223
    :cond_0
    long-to-double v2, p4

    const-wide v4, 0x408f400000000000L

    mul-double/2addr v2, v4

    long-to-double v4, v0

    div-double/2addr v2, v4

    double-to-int v2, v2

    .line 1224
    long-to-double v3, p4

    const-wide v5, 0x40c3880000000000L

    mul-double/2addr v3, v5

    long-to-double v5, v0

    div-double/2addr v3, v5

    double-to-int v3, v3

    rem-int/lit8 v3, v3, 0xa

    .line 1225
    long-to-double v4, p4

    const-wide v6, 0x40f86a0000000000L

    mul-double/2addr v4, v6

    long-to-double v0, v0

    div-double v0, v4, v0

    double-to-int v0, v0

    rem-int/lit8 v0, v0, 0xa

    .line 1226
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " FPS = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 1227
    return-void
.end method

.method private startPinch()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2115
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->reduceWebViewPriority()V

    .line 2116
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    aput v3, v0, v3

    .line 2117
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    aput v3, v0, v4

    .line 2118
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getScale()F

    move-result v1

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getContentWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    aput v1, v0, v3

    .line 2119
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getScale()F

    move-result v1

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getContentHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getTitleHeight()I

    move-result v2

    add-int/2addr v1, v2

    aput v1, v0, v4

    .line 2120
    iput-boolean v4, p0, Lcom/android/browser/WebGLAnimator;->mHasPinched:Z

    .line 2121
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mPinchMotionFilter:Lcom/android/browser/MotionFilter;

    const/high16 v1, 0x4480

    invoke-virtual {v0, v1}, Lcom/android/browser/MotionFilter;->reset(F)V

    .line 2122
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScaleDetector:Lcom/android/browser/DirectScaleGestureDetector;

    invoke-virtual {v0}, Lcom/android/browser/DirectScaleGestureDetector;->update()V

    .line 2123
    iput-boolean v4, p0, Lcom/android/browser/WebGLAnimator;->mPrintFirstPinch:Z

    .line 2124
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/browser/WebGLAnimator;->mPinchFrames:J

    .line 2125
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    invoke-virtual {v0}, Lcom/android/browser/BitmapWebView;->show()V

    .line 2126
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    invoke-virtual {v0}, Lcom/android/browser/BitmapWebView;->requestRender()V

    .line 2127
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->resumeUpdateTextureBitmap()V

    .line 2128
    return-void
.end method

.method private startPinchFromScrollState()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2105
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mTargetZoomScale:F

    .line 2106
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mPinchMotionFilter:Lcom/android/browser/MotionFilter;

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mTargetZoomScale:F

    const/high16 v2, 0x4480

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/android/browser/MotionFilter;->reset(F)V

    .line 2107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/browser/WebGLAnimator;->mHasPinched:Z

    .line 2108
    iput-boolean v3, p0, Lcom/android/browser/WebGLAnimator;->mIsPinchCenterInited:Z

    .line 2109
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScaleDetector:Lcom/android/browser/DirectScaleGestureDetector;

    invoke-virtual {v0}, Lcom/android/browser/DirectScaleGestureDetector;->reset()V

    .line 2110
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mMotionFilter:Lcom/android/browser/MotionFilter;

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mTouchDown:[I

    invoke-virtual {v0, v1}, Lcom/android/browser/MotionFilter;->reset([I)V

    .line 2111
    iput-boolean v3, p0, Lcom/android/browser/WebGLAnimator;->mPrintFirstPinch:Z

    .line 2112
    return-void
.end method

.method private syncPointerDevice(Landroid/view/MotionEvent;)V
    .locals 7
    .parameter "ev"

    .prologue
    .line 282
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v2

    .line 283
    .local v2, started:J
    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mPointerDevice:Landroid/view/PointerDevice;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Landroid/view/PointerDevice;->syncWithMainLooper(J)V

    .line 284
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v0

    .line 285
    .local v0, end:J
    sget-boolean v4, Lcom/android/browser/WebGLAnimator;->DBG:Z

    if-eqz v4, :cond_0

    .line 286
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "syncPointerDevice -- waited for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sub-long v5, v0, v2

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 288
    :cond_0
    return-void
.end method

.method private trackVelocity(Landroid/view/MotionEvent;ZZ)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    const v0, -0x1869f

    .line 2397
    if-eqz p2, :cond_0

    .line 2398
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->obtainNewVelocityTracker()Landroid/view/VelocityTracker;

    .line 2399
    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mLastTrackMoveX:I

    .line 2400
    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mLastTrackMoveY:I

    .line 2402
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 2403
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 2404
    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mLastTrackMoveX:I

    .line 2405
    iput v1, p0, Lcom/android/browser/WebGLAnimator;->mLastTrackMoveY:I

    .line 2407
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_1

    .line 2408
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 2410
    :cond_1
    if-nez p3, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 2411
    :cond_2
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTrackerQuickFlick:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_3

    .line 2412
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTrackerQuickFlick:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 2415
    :cond_3
    sget-boolean v0, Lcom/android/browser/WebGLAnimator;->DBG:Z

    if-eqz v0, :cond_4

    .line 2416
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mVelocityTracker: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->logIfDBG(Ljava/lang/String;)V

    .line 2418
    :cond_4
    return-void
.end method

.method private unimplemented()V
    .locals 1

    .prologue
    .line 2371
    const-string v0, "unimplemented"

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->shouldNotReachHere(Ljava/lang/String;)V

    .line 2372
    return-void
.end method


# virtual methods
.method public abortAnimation()V
    .locals 1

    .prologue
    .line 520
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/browser/WebGLAnimator;->abortAnimation(Z)V

    .line 521
    return-void
.end method

.method public abortAnimation(Z)V
    .locals 2
    .parameter "waitForHide"

    .prologue
    .line 526
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    monitor-enter v0

    .line 527
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, v1}, Lcom/android/browser/WebGLAnimator;->abortAnimationLocked(Z)V

    .line 528
    monitor-exit v0

    .line 529
    return-void

    .line 528
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public declared-synchronized abortAnimationLocked(Z)V
    .locals 4
    .parameter "waitForHide"

    .prologue
    const/4 v2, 0x1

    .line 531
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "abortAnimation "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mState:I

    invoke-direct {p0, v1}, Lcom/android/browser/WebGLAnimator;->getStateName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 532
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mState:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v2, :cond_0

    .line 582
    :goto_0
    monitor-exit p0

    return-void

    .line 535
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/android/browser/WebGLAnimator;->mAbortingAnimation:Z

    .line 536
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mState:I

    sparse-switch v0, :sswitch_data_0

    .line 569
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "did not consider this case: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mState:I

    invoke-direct {p0, v1}, Lcom/android/browser/WebGLAnimator;->getStateName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->shouldNotReachHere(Ljava/lang/String;)V

    .line 572
    :goto_1
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_1

    .line 573
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 574
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 576
    :cond_1
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTrackerQuickFlick:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_2

    .line 577
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTrackerQuickFlick:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 578
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTrackerQuickFlick:Landroid/view/VelocityTracker;

    .line 581
    :cond_2
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/browser/BitmapWebView;->setIsInProgress(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 531
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 538
    :sswitch_0
    const/4 v0, 0x1

    :try_start_2
    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mState:I

    goto :goto_1

    .line 545
    :sswitch_1
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mState:I

    .line 546
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->needBounce()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 549
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mBounceTargetPos:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    aput v2, v0, v1

    .line 550
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mBounceTargetPos:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    aput v2, v0, v1

    .line 551
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mBounceTargetZoomScale:F

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    .line 554
    :cond_3
    if-eqz p1, :cond_4

    .line 555
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->hideLocked()V

    .line 564
    :goto_2
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/browser/BitmapWebView;->initWebViewVariables(ZZ)Z

    .line 565
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->resumeUpdateTextureBitmap()V

    .line 566
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->resumeWebViewPriority()V

    goto :goto_1

    .line 557
    :cond_4
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    invoke-virtual {v0}, Lcom/android/browser/BitmapWebView;->hide()V

    .line 558
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    const/high16 v1, 0x3f80

    cmpl-float v0, v0, v1

    if-nez v0, :cond_5

    .line 559
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->scrollTo(II)V

    goto :goto_2

    .line 561
    :cond_5
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->setWebViewScrollAndZoom()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 536
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x4 -> :sswitch_1
        0x8 -> :sswitch_1
        0x10 -> :sswitch_1
        0x20 -> :sswitch_1
        0x40 -> :sswitch_1
    .end sparse-switch
.end method

.method public assertCalledOnGLThread()V
    .locals 0

    .prologue
    .line 2364
    return-void
.end method

.method public assertCalledOnUIThread()V
    .locals 0

    .prologue
    .line 2368
    return-void
.end method

.method public doDoubleTap(FLandroid/webkit/OnPinchZoomListener$ZoomInfo;)V
    .locals 11
    .parameter
    .parameter

    .prologue
    const/high16 v10, 0x3f80

    const/4 v8, 0x1

    const/4 v7, 0x0

    const-string v9, ", "

    .line 2312
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getMinZoomScale()F

    move-result v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getScale()F

    move-result v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mMinScale:F

    .line 2313
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getMaxZoomScale()F

    move-result v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getScale()F

    move-result v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mMaxScale:F

    .line 2316
    iget v0, p2, Landroid/webkit/OnPinchZoomListener$ZoomInfo;->scale:F

    div-float/2addr v0, p1

    .line 2318
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getScrollX()I

    move-result v2

    aput v2, v1, v7

    .line 2319
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getScrollY()I

    move-result v2

    aput v2, v1, v8

    .line 2325
    iget v1, p2, Landroid/webkit/OnPinchZoomListener$ZoomInfo;->scrollX:F

    div-float/2addr v1, v0

    .line 2326
    iget v2, p2, Landroid/webkit/OnPinchZoomListener$ZoomInfo;->scrollY:F

    div-float/2addr v2, v0

    .line 2329
    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v3, v3, v7

    int-to-float v3, v3

    .line 2330
    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v4, v4, v8

    int-to-float v4, v4

    .line 2332
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/android/browser/WebGLAnimator;->mDTZoomStartMs:J

    .line 2334
    cmpl-float v5, v0, v10

    if-nez v5, :cond_0

    .line 2335
    const v5, 0x3f866666

    iput v5, p0, Lcom/android/browser/WebGLAnimator;->mDTTargetZoomScale:F

    .line 2336
    iput-boolean v8, p0, Lcom/android/browser/WebGLAnimator;->mDTBounceBack:Z

    .line 2337
    iget-object v5, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    iput v5, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterX:F

    .line 2338
    iget-object v5, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v5}, Landroid/webkit/WebView;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    iput v5, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterY:F

    .line 2339
    sget v5, Lcom/android/browser/WebGLAnimator;->ZOOM_BOUNCE_ANIMATION_MS:I

    int-to-long v5, v5

    iput-wide v5, p0, Lcom/android/browser/WebGLAnimator;->mDTZoomDuration:J

    .line 2350
    :goto_0
    iget v5, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterY:F

    iget-object v6, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    invoke-virtual {v6}, Lcom/android/browser/BitmapWebView;->getTitleBarHeight()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    iput v5, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterY:F

    .line 2352
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "doDoubleTap: ratio = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", zoomCenter = ("

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v5, p2, Landroid/webkit/OnPinchZoomListener$ZoomInfo;->zoomCenterX:F

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v5, p2, Landroid/webkit/OnPinchZoomListener$ZoomInfo;->zoomCenterY:F

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ") dst(after) = ("

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v5, p2, Landroid/webkit/OnPinchZoomListener$ZoomInfo;->scrollX:F

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v5, p2, Landroid/webkit/OnPinchZoomListener$ZoomInfo;->scrollY:F

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ") dst(before) = ("

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") src(before) = ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") zoomCenter(before) = ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterX:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterY:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->logIfDBG(Ljava/lang/String;)V

    .line 2359
    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->changeStateTo(I)V

    .line 2360
    return-void

    .line 2341
    :cond_0
    mul-float v5, v1, v0

    sub-float v5, v3, v5

    sub-float v6, v10, v0

    div-float/2addr v5, v6

    iput v5, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterX:F

    .line 2342
    mul-float v5, v2, v0

    sub-float v5, v4, v5

    sub-float v6, v10, v0

    div-float/2addr v5, v6

    iput v5, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterY:F

    .line 2343
    iget v5, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterX:F

    iget-object v6, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v6, v6, v7

    int-to-float v6, v6

    sub-float/2addr v5, v6

    iput v5, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterX:F

    .line 2344
    iget v5, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterY:F

    iget-object v6, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v6, v6, v8

    int-to-float v6, v6

    sub-float/2addr v5, v6

    iput v5, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterY:F

    .line 2345
    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mDTTargetZoomScale:F

    .line 2346
    iput-boolean v7, p0, Lcom/android/browser/WebGLAnimator;->mDTBounceBack:Z

    .line 2347
    sget v5, Lcom/android/browser/WebGLAnimator;->ZOOM_ANIMATION_MS:I

    int-to-long v5, v5

    iput-wide v5, p0, Lcom/android/browser/WebGLAnimator;->mDTZoomDuration:J

    goto/16 :goto_0
.end method

.method getEdgeAnimationStatus()I
    .locals 1

    .prologue
    .line 1188
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeAnimating:I

    return v0
.end method

.method public getScrollX()I
    .locals 2

    .prologue
    .line 304
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public getScrollY()I
    .locals 2

    .prologue
    .line 308
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    return v0
.end method

.method public getTouchDownMs()J
    .locals 2

    .prologue
    .line 2487
    iget-wide v0, p0, Lcom/android/browser/WebGLAnimator;->mTouchDownMs:J

    return-wide v0
.end method

.method public getZoomCenterX()F
    .locals 1

    .prologue
    .line 312
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterX:F

    return v0
.end method

.method public getZoomCenterY()F
    .locals 1

    .prologue
    .line 316
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterY:F

    return v0
.end method

.method public getZoomScale()F
    .locals 1

    .prologue
    .line 320
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    return v0
.end method

.method initPointerDevice()V
    .locals 4

    .prologue
    .line 268
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mPointerDevice:Landroid/view/PointerDevice;

    if-eqz v0, :cond_0

    .line 274
    :goto_0
    return-void

    .line 272
    :cond_0
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/PointerDevice;->obtain(Landroid/content/Context;)Landroid/view/PointerDevice;

    move-result-object v0

    iput-object v0, p0, Lcom/android/browser/WebGLAnimator;->mPointerDevice:Landroid/view/PointerDevice;

    .line 273
    new-instance v0, Lcom/android/browser/DirectScaleGestureDetector;

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/android/browser/WebGLAnimator$ScaleDetectorListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/android/browser/WebGLAnimator$ScaleDetectorListener;-><init>(Lcom/android/browser/WebGLAnimator;Lcom/android/browser/WebGLAnimator$1;)V

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mPointerDevice:Landroid/view/PointerDevice;

    invoke-direct {v0, v1, v2, v3}, Lcom/android/browser/DirectScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/webkit/WebviewScaleGestureDetector$OnScaleGestureListener;Landroid/view/PointerDevice;)V

    iput-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScaleDetector:Lcom/android/browser/DirectScaleGestureDetector;

    goto :goto_0
.end method

.method public declared-synchronized interceptMotionEvent(Landroid/view/MotionEvent;)I
    .locals 12
    .parameter "ev"

    .prologue
    const/4 v8, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 329
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/browser/WebGLAnimator;->assertCalledOnUIThread()V

    .line 330
    invoke-virtual {p0}, Lcom/android/browser/WebGLAnimator;->initPointerDevice()V

    .line 331
    invoke-direct {p0, p1}, Lcom/android/browser/WebGLAnimator;->syncPointerDevice(Landroid/view/MotionEvent;)V

    .line 333
    sget-boolean v6, Lcom/android/browser/WebGLAnimator;->DBG:Z

    if-eqz v6, :cond_0

    .line 334
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "interceptMotionEvent (mState="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/android/browser/WebGLAnimator;->mState:I

    invoke-direct {p0, v7}, Lcom/android/browser/WebGLAnimator;->getStateName(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "): "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", ptrCnt="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 335
    const/4 v5, 0x0

    .local v5, i:I
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v6

    if-ge v5, v6, :cond_0

    .line 336
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "   ptr["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] id="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", x= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", y= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 335
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 341
    .end local v5           #i:I
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 342
    .local v3, ev_x:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 343
    .local v4, ev_y:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 382
    :cond_1
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v6

    if-lt v6, v8, :cond_2

    .line 384
    iget-object v6, p0, Lcom/android/browser/WebGLAnimator;->mActiveTabsPageGestureDetector:Lcom/android/browser/ActiveTabsPageGestureDetector;

    invoke-virtual {v6, p1}, Lcom/android/browser/ActiveTabsPageGestureDetector;->interceptMotionEvent(Landroid/view/MotionEvent;)V

    .line 386
    :cond_2
    iget-boolean v6, p0, Lcom/android/browser/WebGLAnimator;->mAbortingAnimation:Z

    if-eqz v6, :cond_3

    .line 387
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    if-nez v6, :cond_5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v6

    if-ne v6, v10, :cond_5

    .line 388
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/android/browser/WebGLAnimator;->mAbortingAnimation:Z

    .line 397
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v6

    if-le v6, v10, :cond_8

    .line 398
    const/4 v6, 0x3

    invoke-direct {p0, v6}, Lcom/android/browser/WebGLAnimator;->isState(I)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 400
    invoke-direct {p0, p1}, Lcom/android/browser/WebGLAnimator;->getActionEx(Landroid/view/MotionEvent;)I

    move-result v6

    const/16 v7, 0x800

    if-ne v6, v7, :cond_7

    .line 401
    const-string v6, "interceptMotionEvent ACTION_POINTER_UP special case"

    invoke-direct {p0, v6}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 402
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/android/browser/WebGLAnimator;->startScrolling(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v6, v10

    .line 489
    :goto_2
    monitor-exit p0

    return v6

    .line 345
    :pswitch_0
    :try_start_1
    iput v3, p0, Lcom/android/browser/WebGLAnimator;->mFirstTouchPosX:F

    .line 346
    iput v4, p0, Lcom/android/browser/WebGLAnimator;->mFirstTouchPosY:F

    .line 350
    invoke-virtual {p0}, Lcom/android/browser/WebGLAnimator;->resetEdgeAnimationStatus()V

    .line 351
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MotionEvent.ACTION_DOWN: mEdgeToAnimate  = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "mEdgeAnimating="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/android/browser/WebGLAnimator;->mEdgeAnimating:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 329
    .end local v3           #ev_x:F
    .end local v4           #ev_y:F
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 357
    .restart local v3       #ev_x:F
    .restart local v4       #ev_y:F
    :pswitch_1
    :try_start_2
    iget v6, p0, Lcom/android/browser/WebGLAnimator;->mFirstTouchPosX:F

    sub-float v1, v3, v6

    .line 358
    .local v1, dX:F
    iget v6, p0, Lcom/android/browser/WebGLAnimator;->mFirstTouchPosY:F

    sub-float v2, v4, v6

    .line 359
    .local v2, dY:F
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v6

    iget v7, p0, Lcom/android/browser/WebGLAnimator;->mTouchSlop:I

    int-to-float v7, v7

    cmpg-float v6, v6, v7

    if-gez v6, :cond_1

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v6

    iget v7, p0, Lcom/android/browser/WebGLAnimator;->mTouchSlop:I

    int-to-float v7, v7

    cmpg-float v6, v6, v7

    if-gez v6, :cond_1

    .line 361
    sget-boolean v6, Lcom/android/browser/WebGLAnimator;->DBG:Z

    if-eqz v6, :cond_4

    .line 362
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "interceptMotionEvent motion movement is too small! ignore it! mFirstTouchPosX="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/android/browser/WebGLAnimator;->mFirstTouchPosX:F

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", mFirstTouchPosY="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/android/browser/WebGLAnimator;->mFirstTouchPosY:F

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " , ev_x="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", ev_y="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " , dX="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", dY="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", slop="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/android/browser/WebGLAnimator;->mTouchSlop:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 367
    :cond_4
    const/4 v6, -0x1

    goto/16 :goto_2

    .line 372
    .end local v1           #dX:F
    .end local v2           #dY:F
    :pswitch_2
    const/4 v6, 0x0

    iput v6, p0, Lcom/android/browser/WebGLAnimator;->mFirstTouchPosX:F

    .line 373
    const/4 v6, 0x0

    iput v6, p0, Lcom/android/browser/WebGLAnimator;->mFirstTouchPosY:F

    goto/16 :goto_1

    .line 390
    :cond_5
    sget-boolean v6, Lcom/android/browser/WebGLAnimator;->DBG:Z

    if-eqz v6, :cond_6

    .line 391
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Aborting: ignore this event: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    :cond_6
    move v6, v11

    .line 392
    goto/16 :goto_2

    :cond_7
    move v6, v11

    .line 408
    goto/16 :goto_2

    .line 412
    :cond_8
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    if-nez v6, :cond_9

    .line 413
    iget-object v6, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    iget-object v7, p0, Lcom/android/browser/WebGLAnimator;->mLocationOnScreen:[I

    invoke-virtual {v6, v7}, Landroid/webkit/WebView;->getLocationOnScreen([I)V

    .line 414
    iget-object v6, p0, Lcom/android/browser/WebGLAnimator;->mPointerDevice:Landroid/view/PointerDevice;

    iget-object v7, p0, Lcom/android/browser/WebGLAnimator;->mLocationOnScreen:[I

    const/4 v8, 0x0

    aget v7, v7, v8

    iget-object v8, p0, Lcom/android/browser/WebGLAnimator;->mLocationOnScreen:[I

    const/4 v9, 0x1

    aget v8, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/view/PointerDevice;->setCoordOrigin(II)V

    .line 417
    :cond_9
    invoke-direct {p0, p1}, Lcom/android/browser/WebGLAnimator;->getActionEx(Landroid/view/MotionEvent;)I

    move-result v0

    .line 418
    .local v0, actionEX:I
    iget v6, p0, Lcom/android/browser/WebGLAnimator;->mState:I

    or-int/2addr v6, v0

    sparse-switch v6, :sswitch_data_0

    .line 480
    const/16 v6, 0x400

    if-ne v0, v6, :cond_a

    .line 481
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/android/browser/WebGLAnimator;->mSecondPointerDownMs:J

    .line 483
    :cond_a
    iget v6, p0, Lcom/android/browser/WebGLAnimator;->mState:I

    if-ne v6, v10, :cond_b

    move v6, v11

    .line 484
    goto/16 :goto_2

    .line 420
    :sswitch_0
    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-direct {p0, p1, v6, v7}, Lcom/android/browser/WebGLAnimator;->trackVelocity(Landroid/view/MotionEvent;ZZ)V

    .line 421
    const/4 v6, 0x2

    invoke-direct {p0, v6}, Lcom/android/browser/WebGLAnimator;->changeStateTo(I)V

    .line 422
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/android/browser/WebGLAnimator;->mSecondPointerDownMs:J

    move v6, v11

    .line 423
    goto/16 :goto_2

    .line 426
    :sswitch_1
    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct {p0, p1, v6, v7}, Lcom/android/browser/WebGLAnimator;->trackVelocity(Landroid/view/MotionEvent;ZZ)V

    move v6, v11

    .line 427
    goto/16 :goto_2

    .line 430
    :sswitch_2
    const/4 v6, 0x1

    invoke-direct {p0, v6}, Lcom/android/browser/WebGLAnimator;->changeStateTo(I)V

    move v6, v11

    .line 431
    goto/16 :goto_2

    .line 438
    :sswitch_3
    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct {p0, p1, v6, v7}, Lcom/android/browser/WebGLAnimator;->trackVelocity(Landroid/view/MotionEvent;ZZ)V

    .line 439
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->needFling()Z

    move-result v6

    if-eqz v6, :cond_d

    .line 440
    const/16 v6, 0x8

    invoke-direct {p0, v6}, Lcom/android/browser/WebGLAnimator;->changeStateTo(I)V

    .line 488
    :cond_b
    :goto_3
    :sswitch_4
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->isWebViewFixedWidth()Z

    move-result v6

    if-nez v6, :cond_c

    :cond_c
    move v6, v11

    .line 489
    goto/16 :goto_2

    .line 441
    :cond_d
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->needBounce()Z

    move-result v6

    if-eqz v6, :cond_e

    .line 442
    const/16 v6, 0x40

    invoke-direct {p0, v6}, Lcom/android/browser/WebGLAnimator;->changeStateTo(I)V

    goto :goto_3

    .line 444
    :cond_e
    const/4 v6, 0x1

    invoke-direct {p0, v6}, Lcom/android/browser/WebGLAnimator;->changeStateTo(I)V

    .line 445
    iget-boolean v6, p0, Lcom/android/browser/WebGLAnimator;->mHasPinched:Z

    if-nez v6, :cond_b

    move v6, v11

    .line 446
    goto/16 :goto_2

    .line 456
    :sswitch_5
    invoke-virtual {p0}, Lcom/android/browser/WebGLAnimator;->restartScrolling()V

    move v6, v10

    .line 457
    goto/16 :goto_2

    .line 460
    :sswitch_6
    invoke-virtual {p0}, Lcom/android/browser/WebGLAnimator;->restartScrolling()V

    move v6, v10

    .line 461
    goto/16 :goto_2

    .line 464
    :sswitch_7
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->needBounce()Z

    move-result v6

    if-nez v6, :cond_f

    .line 465
    const/4 v6, 0x1

    invoke-direct {p0, v6}, Lcom/android/browser/WebGLAnimator;->changeStateTo(I)V

    goto :goto_3

    .line 467
    :cond_f
    const/16 v6, 0x40

    invoke-direct {p0, v6}, Lcom/android/browser/WebGLAnimator;->changeStateTo(I)V

    goto :goto_3

    .line 472
    :sswitch_8
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->isWebViewFixedWidth()Z

    move-result v6

    if-nez v6, :cond_10

    .line 473
    const/16 v6, 0x10

    invoke-direct {p0, v6}, Lcom/android/browser/WebGLAnimator;->changeStateTo(I)V

    :goto_4
    move v6, v10

    .line 477
    goto/16 :goto_2

    .line 475
    :cond_10
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/android/browser/WebGLAnimator;->mTouchDownStale:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    .line 343
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    .line 418
    :sswitch_data_0
    .sparse-switch
        0x81 -> :sswitch_0
        0x88 -> :sswitch_5
        0xa0 -> :sswitch_5
        0xc0 -> :sswitch_5
        0x102 -> :sswitch_2
        0x104 -> :sswitch_3
        0x108 -> :sswitch_4
        0x110 -> :sswitch_7
        0x202 -> :sswitch_1
        0x404 -> :sswitch_8
        0x810 -> :sswitch_6
    .end sparse-switch
.end method

.method public isAbortingAnimation()Z
    .locals 1

    .prologue
    .line 496
    iget-boolean v0, p0, Lcom/android/browser/WebGLAnimator;->mAbortingAnimation:Z

    return v0
.end method

.method isShowScrollbar()Z
    .locals 2

    .prologue
    .line 745
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mState:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mState:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public needEdgeAnimation()I
    .locals 12

    .prologue
    const/4 v7, -0x1

    const/high16 v10, 0x41f0

    const/4 v9, 0x1

    const/4 v8, 0x0

    const-string v11, "mEdgeAnimating="

    .line 1005
    .line 1008
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->isState(I)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1010
    const/16 v0, 0xa

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mVariableEdgeWidth:I

    .line 1011
    iget-boolean v0, p0, Lcom/android/browser/WebGLAnimator;->bFlingToEdge:Z

    if-ne v0, v9, :cond_6

    .line 1012
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v0, v0, v8

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    aget v1, v1, v8

    if-gt v0, v1, :cond_21

    .line 1013
    or-int/lit8 v0, v8, 0x1

    .line 1015
    :goto_0
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v1, v1, v8

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v2, v2, v8

    if-lt v1, v2, :cond_0

    .line 1016
    or-int/lit8 v0, v0, 0x2

    .line 1018
    :cond_0
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v1, v1, v9

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    aget v2, v2, v9

    if-gt v1, v2, :cond_1

    .line 1019
    or-int/lit8 v0, v0, 0x4

    .line 1021
    :cond_1
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v1, v1, v9

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v2, v2, v9

    if-lt v1, v2, :cond_2

    .line 1022
    or-int/lit8 v0, v0, 0x8

    .line 1024
    :cond_2
    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mVariableEdgeWidth:I

    const/16 v2, 0x1e

    if-ge v1, v2, :cond_3

    .line 1025
    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mVariableEdgeWidth:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/browser/WebGLAnimator;->mVariableEdgeWidth:I

    .line 1027
    :cond_3
    iput v8, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    .line 1028
    if-eqz v0, :cond_4

    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeAnimating:I

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    if-ne v0, v1, :cond_4

    .line 1031
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "needEdgeAnimation(-1,0): mEdgeToAnimate  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mEdgeAnimating="

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mEdgeAnimating:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    move v0, v7

    .line 1176
    :cond_5
    :goto_1
    return v0

    .line 1035
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "needEdgeAnimation(-1): mEdgeToAnimate  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mEdgeAnimating="

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mEdgeAnimating:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    move v0, v7

    .line 1036
    goto :goto_1

    .line 1037
    :cond_7
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->isState(I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1038
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "needEdgeAnimation(STATE_PINCH): mEdgeToAnimate  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mEdgeAnimating="

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mEdgeAnimating:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 1039
    iput v8, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    move v0, v7

    .line 1040
    goto :goto_1

    .line 1043
    :cond_8
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_1e

    .line 1044
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 1045
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    .line 1046
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v1

    .line 1047
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 1048
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 1052
    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    iget v4, v4, Lcom/android/browser/BitmapWebView;->mViewHeight:I

    iget-object v5, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    iget v5, v5, Lcom/android/browser/BitmapWebView;->mContentsHeight:I

    if-ge v4, v5, :cond_20

    move v4, v9

    .line 1055
    :goto_2
    iget-object v5, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    iget v5, v5, Lcom/android/browser/BitmapWebView;->mViewWidth:I

    iget-object v6, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    iget v6, v6, Lcom/android/browser/BitmapWebView;->mContentsWidth:I

    if-ge v5, v6, :cond_1f

    move v5, v9

    .line 1059
    :goto_3
    const/high16 v6, 0x4220

    cmpl-float v6, v2, v6

    if-gtz v6, :cond_9

    const/high16 v6, 0x4220

    cmpl-float v6, v3, v6

    if-lez v6, :cond_16

    .line 1060
    :cond_9
    if-eqz v5, :cond_a

    .line 1061
    iget-object v6, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v6, v6, v8

    iget-object v7, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    aget v7, v7, v8

    if-gt v6, v7, :cond_11

    .line 1062
    cmpg-float v6, v2, v3

    if-gez v6, :cond_a

    const/4 v6, 0x0

    cmpl-float v6, v1, v6

    if-lez v6, :cond_a

    .line 1063
    iget v6, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    .line 1064
    if-eqz v4, :cond_a

    .line 1065
    sub-float v6, v0, v1

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpg-float v6, v6, v10

    if-gez v6, :cond_10

    .line 1066
    iget v6, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    .line 1085
    :cond_a
    :goto_4
    if-eqz v4, :cond_b

    .line 1086
    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v4, v4, v9

    iget-object v6, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    aget v6, v6, v9

    if-gt v4, v6, :cond_14

    .line 1087
    cmpl-float v2, v2, v3

    if-lez v2, :cond_b

    const/4 v2, 0x0

    cmpl-float v2, v0, v2

    if-lez v2, :cond_b

    .line 1088
    iget v2, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    .line 1089
    if-eqz v5, :cond_b

    .line 1090
    sub-float v2, v0, v1

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v2, v2, v10

    if-gez v2, :cond_13

    .line 1091
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    .line 1112
    :cond_b
    :goto_5
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->isState(I)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1113
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mFlingEdgeGlowStartMs:[J

    aget-wide v0, v0, v8

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_c

    .line 1114
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    and-int/lit8 v0, v0, 0x3

    if-eqz v0, :cond_c

    .line 1115
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mFlingEdgeGlowStartMs:[J

    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v1

    aput-wide v1, v0, v8

    .line 1117
    :cond_c
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mFlingEdgeGlowStartMs:[J

    aget-wide v0, v0, v9

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_d

    .line 1118
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    and-int/lit8 v0, v0, 0xc

    if-eqz v0, :cond_d

    .line 1119
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mFlingEdgeGlowStartMs:[J

    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v1

    aput-wide v1, v0, v9

    .line 1123
    :cond_d
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "needEdgeAnimation(1): mEdgeToAnimate  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mEdgeAnimating="

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mEdgeAnimating:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 1124
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeAnimating:I

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    if-eq v0, v1, :cond_e

    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeAnimating:I

    if-lez v0, :cond_e

    .line 1125
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeAnimating:I

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    .line 1164
    :cond_e
    :goto_6
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mVariableEdgeWidth:I

    const/16 v1, 0x1e

    if-ge v0, v1, :cond_f

    .line 1166
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mVariableEdgeWidth:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mVariableEdgeWidth:I

    .line 1168
    :cond_f
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    .line 1169
    if-eqz v0, :cond_5

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mEdgeAnimating:I

    iget v2, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    if-ne v1, v2, :cond_5

    .line 1170
    const/16 v0, 0x10

    goto/16 :goto_1

    .line 1067
    :cond_10
    neg-float v6, v0

    sub-float/2addr v6, v1

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpg-float v6, v6, v10

    if-gez v6, :cond_a

    .line 1068
    iget v6, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    or-int/lit8 v6, v6, 0x8

    iput v6, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    goto/16 :goto_4

    .line 1072
    :cond_11
    iget-object v6, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v6, v6, v8

    iget-object v7, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v7, v7, v8

    if-lt v6, v7, :cond_a

    .line 1073
    cmpg-float v6, v2, v3

    if-gez v6, :cond_a

    const/4 v6, 0x0

    cmpg-float v6, v1, v6

    if-gez v6, :cond_a

    .line 1074
    iget v6, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    .line 1075
    if-eqz v4, :cond_a

    .line 1076
    add-float v6, v0, v1

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpg-float v6, v6, v10

    if-gez v6, :cond_12

    .line 1077
    iget v6, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    or-int/lit8 v6, v6, 0x4

    iput v6, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    goto/16 :goto_4

    .line 1078
    :cond_12
    neg-float v6, v0

    add-float/2addr v6, v1

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpg-float v6, v6, v10

    if-gez v6, :cond_a

    .line 1079
    iget v6, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    or-int/lit8 v6, v6, 0x8

    iput v6, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    goto/16 :goto_4

    .line 1092
    :cond_13
    add-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v10

    if-gez v0, :cond_b

    .line 1093
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    goto/16 :goto_5

    .line 1097
    :cond_14
    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v4, v4, v9

    iget-object v6, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v6, v6, v9

    if-lt v4, v6, :cond_b

    .line 1098
    cmpl-float v2, v2, v3

    if-lez v2, :cond_b

    const/4 v2, 0x0

    cmpg-float v2, v0, v2

    if-gez v2, :cond_b

    .line 1099
    iget v2, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    .line 1100
    if-eqz v5, :cond_b

    .line 1101
    neg-float v2, v0

    sub-float/2addr v2, v1

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v2, v2, v10

    if-gez v2, :cond_15

    .line 1102
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    goto/16 :goto_5

    .line 1103
    :cond_15
    neg-float v0, v0

    add-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v10

    if-gez v0, :cond_b

    .line 1104
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    goto/16 :goto_5

    .line 1127
    :cond_16
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->isState(I)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1128
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeAnimating:I

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    .line 1129
    iput-boolean v9, p0, Lcom/android/browser/WebGLAnimator;->mCheckScrollforEdge:Z

    .line 1131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "needEdgeAnimation(STATE_SCROLL): mEdgeToAnimate  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mEdgeAnimating="

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mEdgeAnimating:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 1132
    :cond_17
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->isState(I)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1133
    iget-boolean v0, p0, Lcom/android/browser/WebGLAnimator;->mCheckScrollforEdge:Z

    if-eqz v0, :cond_18

    .line 1134
    iput v8, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    move v0, v7

    .line 1136
    goto/16 :goto_1

    .line 1139
    :cond_18
    if-eqz v5, :cond_19

    .line 1140
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v0, v0, v8

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    aget v1, v1, v8

    if-gt v0, v1, :cond_1b

    .line 1141
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    .line 1147
    :cond_19
    :goto_7
    if-eqz v4, :cond_1a

    .line 1148
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v0, v0, v9

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    aget v1, v1, v9

    if-gt v0, v1, :cond_1c

    .line 1149
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    .line 1155
    :cond_1a
    :goto_8
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->isState(I)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 1156
    iput-boolean v9, p0, Lcom/android/browser/WebGLAnimator;->bFlingToEdge:Z

    .line 1157
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "needEdgeAnimation(STATE_FLING): mEdgeToAnimate  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mEdgeAnimating="

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mEdgeAnimating:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 1162
    :goto_9
    const/16 v0, 0x1d

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mVariableEdgeWidth:I

    goto/16 :goto_6

    .line 1142
    :cond_1b
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v0, v0, v8

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v1, v1, v8

    if-lt v0, v1, :cond_19

    .line 1143
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    goto :goto_7

    .line 1150
    :cond_1c
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v0, v0, v9

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v1, v1, v9

    if-lt v0, v1, :cond_1a

    .line 1151
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    goto :goto_8

    .line 1160
    :cond_1d
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "needEdgeAnimation(STATE_PINCH): mEdgeToAnimate  = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mEdgeAnimating="

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mEdgeAnimating:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    goto :goto_9

    .line 1175
    :cond_1e
    iput v8, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    move v0, v7

    .line 1176
    goto/16 :goto_1

    :cond_1f
    move v5, v8

    goto/16 :goto_3

    :cond_20
    move v4, v8

    goto/16 :goto_2

    :cond_21
    move v0, v8

    goto/16 :goto_0
.end method

.method public declared-synchronized onDrawFrameEnd()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2091
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/android/browser/WebGLAnimator;->mDrawingLastFrame:Z

    if-eqz v0, :cond_0

    .line 2092
    const-string v0, "onDrawFrameEnd: mDrawingLastFrame = true"

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->logIfDBG(Ljava/lang/String;)V

    .line 2094
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/browser/WebGLAnimator;->mDrawingLastFrame:Z

    .line 2095
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->resumeUpdateTextureBitmap()V

    .line 2097
    :cond_0
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->isState(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 2100
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 2091
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onDrawFrameStart()Z
    .locals 13

    .prologue
    const-wide/16 v11, 0xa

    const/4 v10, 0x0

    const-wide/16 v8, 0x0

    .line 1450
    monitor-enter p0

    .line 1451
    :try_start_0
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/browser/WebGLAnimator;->mGLWakeupTime:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 1452
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/browser/WebGLAnimator;->mGLHasPaused:Z

    .line 1453
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 1454
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v0

    .line 1456
    const-string v2, "onDrawFrameStart: sleeping"

    invoke-direct {p0, v2}, Lcom/android/browser/WebGLAnimator;->logIfDBG(Ljava/lang/String;)V

    .line 1459
    :goto_0
    iget-wide v2, p0, Lcom/android/browser/WebGLAnimator;->mGLWakeupTime:J

    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v4

    sub-long/2addr v2, v4

    cmp-long v4, v2, v8

    if-lez v4, :cond_0

    .line 1460
    iget-boolean v4, p0, Lcom/android/browser/WebGLAnimator;->mWebViewDone:Z

    if-eqz v4, :cond_3

    iget-wide v4, p0, Lcom/android/browser/WebGLAnimator;->mGLWakeupTimeMin:J

    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-gtz v4, :cond_3

    .line 1461
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mGLWakeupTimeMin - now() = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/android/browser/WebGLAnimator;->mGLWakeupTimeMin:J

    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v7

    sub-long/2addr v5, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/android/browser/WebGLAnimator;->logIfDBG(Ljava/lang/String;)V

    .line 1473
    :cond_0
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/android/browser/WebGLAnimator;->mGLWakeupTime:J

    .line 1474
    iget-boolean v4, p0, Lcom/android/browser/WebGLAnimator;->mWebViewDone:Z

    if-eqz v4, :cond_1

    .line 1475
    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Landroid/os/SystemClock;->sleep(J)V

    .line 1477
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onDrawFrameStart: wake up, slept for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v5

    sub-long v0, v5, v0

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms, mWebViewDone = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/browser/WebGLAnimator;->mWebViewDone:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sleepTime = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->logIfDBG(Ljava/lang/String;)V

    .line 1480
    :cond_2
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->isState(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1481
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/browser/WebGLAnimator;->mCheckScrollforEdge:Z

    .line 1482
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onDrawFrameStart: do nothing in state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mState:I

    invoke-direct {p0, v1}, Lcom/android/browser/WebGLAnimator;->getStateName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 1483
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v10

    .line 1486
    :goto_1
    return v0

    .line 1464
    :cond_3
    cmp-long v4, v2, v11

    if-lez v4, :cond_4

    move-wide v2, v11

    .line 1468
    :cond_4
    :try_start_1
    invoke-virtual {p0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 1469
    :catch_0
    move-exception v2

    .line 1470
    :try_start_2
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_0

    .line 1487
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1486
    :cond_5
    :try_start_3
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->animateLocked()Z

    move-result v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method onEdgeAnimationStart(I)V
    .locals 2
    .parameter

    .prologue
    .line 1195
    iput p1, p0, Lcom/android/browser/WebGLAnimator;->mEdgeAnimating:I

    .line 1196
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onEdgeAnimationStart: mEdgeAnimating="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mEdgeAnimating:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 1197
    return-void
.end method

.method public onPaused()V
    .locals 1

    .prologue
    .line 516
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mState:I

    .line 517
    return-void
.end method

.method public onPinchZoomListenerNewScale()V
    .locals 2

    .prologue
    .line 2281
    invoke-virtual {p0}, Lcom/android/browser/WebGLAnimator;->assertCalledOnUIThread()V

    .line 2283
    iget-boolean v0, p0, Lcom/android/browser/WebGLAnimator;->mAbortingAnimation:Z

    if-eqz v0, :cond_1

    .line 2284
    const-string v0, "Aborting: onPinchZoomListenerNewScale"

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 2294
    :cond_0
    :goto_0
    return-void

    .line 2288
    :cond_1
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->isState(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2290
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getMaxZoomScale()F

    move-result v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getMinZoomScale()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/browser/WebGLAnimator;->onPinchZoomListenerScaleBegin(FF)V

    goto :goto_0
.end method

.method public declared-synchronized onPinchZoomListenerScaleBegin(FF)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 2244
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/browser/WebGLAnimator;->assertCalledOnUIThread()V

    .line 2245
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onPinchZoomListenerScaleBegin maxWebViewScale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", minWebViewScale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", WebViewScale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getScale()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->logIfDBG(Ljava/lang/String;)V

    .line 2248
    iget-boolean v0, p0, Lcom/android/browser/WebGLAnimator;->mAbortingAnimation:Z

    if-eqz v0, :cond_0

    .line 2249
    const-string v0, "Aborting: onPinchZoomListenerScaleBegin"

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2274
    :goto_0
    monitor-exit p0

    return-void

    .line 2253
    :cond_0
    const/4 v0, 0x3

    :try_start_1
    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->isState(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2254
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->isWebViewFixedWidth()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2257
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getScrollX()I

    move-result v2

    aput v2, v0, v1

    .line 2258
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getScrollY()I

    move-result v2

    aput v2, v0, v1

    .line 2262
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getScale()F

    move-result v0

    div-float v0, p2, v0

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mMinScale:F

    .line 2263
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getScale()F

    move-result v0

    div-float v0, p1, v0

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mMaxScale:F

    .line 2265
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->changeStateTo(I)V

    .line 2270
    :cond_1
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mActiveTabsPageGestureDetector:Lcom/android/browser/ActiveTabsPageGestureDetector;

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getScale()F

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/android/browser/ActiveTabsPageGestureDetector;->onScaleBegin(FFF)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2244
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2272
    :cond_2
    :try_start_2
    const-string v0, "events should have been intercepted"

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->shouldNotReachHere(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public onPinchZoomListenerScaleEnd()V
    .locals 1

    .prologue
    .line 2301
    const-string v0, "event should have been intercepted"

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->shouldNotReachHere(Ljava/lang/String;)V

    .line 2302
    return-void
.end method

.method public declared-synchronized onWebViewDrawFinished()V
    .locals 6

    .prologue
    const-wide/16 v3, 0x0

    .line 1426
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onWebViewDrawFinished: mZoomView.isVisible = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    invoke-virtual {v1}, Lcom/android/browser/BitmapWebView;->isVisible()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->logIfDBG(Ljava/lang/String;)V

    .line 1427
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/browser/WebGLAnimator;->mLastWebViewDrawFinishedMs:J

    .line 1428
    iget-boolean v0, p0, Lcom/android/browser/WebGLAnimator;->mWebViewDone:Z

    if-nez v0, :cond_0

    .line 1430
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/browser/WebGLAnimator;->mWebViewDone:Z

    .line 1431
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 1433
    :cond_0
    iget-wide v0, p0, Lcom/android/browser/WebGLAnimator;->mHideLockedStartMs:J

    cmp-long v0, v0, v3

    if-lez v0, :cond_2

    .line 1434
    const-string v0, ""

    .line 1435
    iget-wide v1, p0, Lcom/android/browser/WebGLAnimator;->mBounceStartMs:J

    cmp-long v1, v1, v3

    if-lez v1, :cond_1

    .line 1436
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "; from finger lift = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/browser/WebGLAnimator;->mLastWebViewDrawFinishedMs:J

    iget-wide v3, p0, Lcom/android/browser/WebGLAnimator;->mBounceStartMs:J

    sub-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1438
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onWebViewDrawFinished: hideLocked() completed in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/browser/WebGLAnimator;->mLastWebViewDrawFinishedMs:J

    iget-wide v4, p0, Lcom/android/browser/WebGLAnimator;->mHideLockedStartMs:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 1439
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/browser/WebGLAnimator;->mHideLockedStartMs:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1441
    :cond_2
    monitor-exit p0

    return-void

    .line 1426
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public refrainUpdateTextureBitmap()V
    .locals 1

    .prologue
    .line 1284
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    invoke-virtual {v0}, Lcom/android/browser/BitmapWebView;->refrainUpdateContents()V

    .line 1285
    return-void
.end method

.method public registerGLThread()V
    .locals 2

    .prologue
    .line 2390
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v0

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mGlThreadID:I

    .line 2391
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mGlThreadID = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/browser/WebGLAnimator;->mGlThreadID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->logIfDBG(Ljava/lang/String;)V

    .line 2392
    return-void
.end method

.method resetEdgeAnimationStatus()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 1203
    iput v1, p0, Lcom/android/browser/WebGLAnimator;->mEdgeAnimating:I

    .line 1204
    iput v1, p0, Lcom/android/browser/WebGLAnimator;->mEdgeToAnimate:I

    .line 1205
    const/16 v0, 0xa

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mVariableEdgeWidth:I

    .line 1206
    iput-boolean v1, p0, Lcom/android/browser/WebGLAnimator;->bFlingToEdge:Z

    .line 1207
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mFlingEdgeGlowStartMs:[J

    aput-wide v2, v0, v1

    .line 1208
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mFlingEdgeGlowStartMs:[J

    const/4 v1, 0x1

    aput-wide v2, v0, v1

    .line 1209
    const-string v0, "resetEdgeAnimationStatus:"

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 1210
    return-void
.end method

.method public restartScrolling()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1231
    invoke-virtual {p0}, Lcom/android/browser/WebGLAnimator;->assertCalledOnUIThread()V

    .line 1233
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomView:Lcom/android/browser/BitmapWebView;

    invoke-virtual {v0}, Lcom/android/browser/BitmapWebView;->isMobilePage()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1234
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->resumeUpdateTextureBitmap()V

    .line 1237
    :cond_0
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->checkWebViewScrollAndZoom()Z

    .line 1238
    sget-boolean v0, Lcom/android/browser/WebGLAnimator;->DBG:Z

    if-eqz v0, :cond_1

    .line 1239
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "restartScrolling: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->getConfigString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->log(Ljava/lang/String;)V

    .line 1241
    :cond_1
    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollStartMs:J

    .line 1243
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollStart:[I

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v1, v1, v2

    aput v1, v0, v2

    .line 1244
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollStart:[I

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v1, v1, v3

    aput v1, v0, v3

    .line 1255
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v0, v0, v2

    if-gez v0, :cond_2

    .line 1256
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aput v2, v0, v2

    .line 1258
    :cond_2
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v0, v0, v3

    if-gez v0, :cond_3

    .line 1259
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aput v2, v0, v3

    .line 1262
    :cond_3
    iput-boolean v2, p0, Lcom/android/browser/WebGLAnimator;->mStationary:Z

    .line 1263
    iput v2, p0, Lcom/android/browser/WebGLAnimator;->mSnapScrollMode:I

    .line 1264
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollFrames:J

    .line 1265
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollStartScale:F

    .line 1267
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mTouchDown:[I

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->getOneFingerXY([I)Z

    .line 1268
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mMotionFilter:Lcom/android/browser/MotionFilter;

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mTouchDown:[I

    invoke-virtual {v0, v1}, Lcom/android/browser/MotionFilter;->reset([I)V

    move v0, v2

    .line 1271
    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_6

    .line 1272
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v1, v1, v0

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    aget v2, v2, v0

    if-ge v1, v2, :cond_5

    .line 1273
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mTouchDown:[I

    aget v2, v1, v0

    int-to-float v2, v2

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    aget v3, v3, v0

    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v4, v4, v0

    sub-int/2addr v3, v4

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget v4, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-int v2, v2

    aput v2, v1, v0

    .line 1274
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollStart:[I

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    aget v2, v2, v0

    aput v2, v1, v0

    .line 1271
    :cond_4
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1275
    :cond_5
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v1, v1, v0

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v2, v2, v0

    if-le v1, v2, :cond_4

    .line 1276
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mTouchDown:[I

    aget v2, v1, v0

    int-to-float v2, v2

    iget-object v3, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    aget v3, v3, v0

    iget-object v4, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v4, v4, v0

    sub-int/2addr v3, v4

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget v4, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    aput v2, v1, v0

    .line 1277
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollStart:[I

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v2, v2, v0

    aput v2, v1, v0

    goto :goto_1

    .line 1280
    :cond_6
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->changeStateTo(I)V

    .line 1281
    return-void
.end method

.method public setWebView(Landroid/webkit/WebView;)V
    .locals 0
    .parameter "webview"

    .prologue
    .line 291
    iput-object p1, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    .line 292
    invoke-virtual {p0}, Lcom/android/browser/WebGLAnimator;->initPointerDevice()V

    .line 293
    return-void
.end method

.method public startScrolling()V
    .locals 1

    .prologue
    .line 1315
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/browser/WebGLAnimator;->startScrolling(Z)V

    .line 1316
    return-void
.end method

.method startScrolling(Z)V
    .locals 10
    .parameter

    .prologue
    const/4 v9, 0x4

    const/high16 v8, 0x3fc0

    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1347
    invoke-virtual {p0}, Lcom/android/browser/WebGLAnimator;->assertCalledOnUIThread()V

    .line 1348
    iget v0, p0, Lcom/android/browser/WebGLAnimator;->mState:I

    sparse-switch v0, :sswitch_data_0

    .line 1400
    :goto_0
    :sswitch_0
    return-void

    .line 1353
    :sswitch_1
    iput-boolean v7, p0, Lcom/android/browser/WebGLAnimator;->mTouchDownStale:Z

    .line 1356
    :sswitch_2
    iput-boolean v6, p0, Lcom/android/browser/WebGLAnimator;->mHasPinched:Z

    .line 1357
    const/high16 v0, 0x3f80

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mZoomScale:F

    .line 1358
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getMaxZoomScale()F

    move-result v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getScale()F

    move-result v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mMaxScale:F

    .line 1359
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getMinZoomScale()F

    move-result v0

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getScale()F

    move-result v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/browser/WebGLAnimator;->mMinScale:F

    .line 1360
    iput v2, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterX:F

    .line 1361
    iput v2, p0, Lcom/android/browser/WebGLAnimator;->mZoomCenterY:F

    .line 1362
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollStart:[I

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getScrollX()I

    move-result v2

    aput v2, v1, v6

    aput v2, v0, v6

    .line 1363
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollStart:[I

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mScrollPos:[I

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getScrollY()I

    move-result v2

    aput v2, v1, v7

    aput v2, v0, v7

    .line 1364
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    aput v6, v0, v6

    .line 1365
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollMin:[I

    aput v6, v0, v7

    .line 1366
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getScale()F

    move-result v1

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getContentWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    aput v1, v0, v6

    .line 1367
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getScale()F

    move-result v1

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getContentHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getTitleHeight()I

    move-result v2

    add-int/2addr v1, v2

    aput v1, v0, v7

    .line 1368
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v0, v0, v6

    if-gez v0, :cond_0

    .line 1369
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aput v6, v0, v6

    .line 1371
    :cond_0
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aget v0, v0, v7

    if-gez v0, :cond_1

    .line 1372
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mScrollMax:[I

    aput v6, v0, v7

    .line 1374
    :cond_1
    iget-boolean v0, p0, Lcom/android/browser/WebGLAnimator;->mTouchDownStale:Z

    if-eqz v0, :cond_2

    .line 1375
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mPointerDevice:Landroid/view/PointerDevice;

    const-wide v1, 0x7fffffffffffffffL

    invoke-virtual {v0, v1, v2}, Landroid/view/PointerDevice;->syncWithMainLooper(J)V

    .line 1376
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mTouchDown:[I

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->getOneFingerXY([I)Z

    .line 1378
    :cond_2
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mMotionFilter:Lcom/android/browser/MotionFilter;

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mTouchDown:[I

    invoke-virtual {v0, v1}, Lcom/android/browser/MotionFilter;->reset([I)V

    .line 1380
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mLastUIMovePos:[I

    invoke-direct {p0, v0}, Lcom/android/browser/WebGLAnimator;->getOneFingerXY([I)Z

    .line 1381
    iget-object v0, p0, Lcom/android/browser/WebGLAnimator;->mLastUIMovePos:[I

    aget v0, v0, v6

    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mTouchDown:[I

    aget v1, v1, v6

    sub-int/2addr v0, v1

    .line 1382
    iget-object v1, p0, Lcom/android/browser/WebGLAnimator;->mLastUIMovePos:[I

    aget v1, v1, v7

    iget-object v2, p0, Lcom/android/browser/WebGLAnimator;->mTouchDown:[I

    aget v2, v2, v7

    sub-int/2addr v1, v2

    .line 1384
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 1385
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 1386
    int-to-float v4, v2

    int-to-float v5, v3

    mul-float/2addr v5, v8

    cmpl-float v4, v4, v5

    if-lez v4, :cond_4

    invoke-direct {p0}, Lcom/android/browser/WebGLAnimator;->isWebViewFixedWidth()Z

    move-result v4

    if-nez v4, :cond_4

    .line 1387
    const/4 v1, 0x2

    iput v1, p0, Lcom/android/browser/WebGLAnimator;->mSnapScrollMode:I

    .line 1388
    if-lez v0, :cond_3

    move v0, v7

    :goto_1
    iput-boolean v0, p0, Lcom/android/browser/WebGLAnimator;->mSnapPositive:Z

    .line 1396
    :goto_2
    invoke-direct {p0, v9}, Lcom/android/browser/WebGLAnimator;->changeStateTo(I)V

    goto/16 :goto_0

    :cond_3
    move v0, v6

    .line 1388
    goto :goto_1

    .line 1389
    :cond_4
    int-to-float v0, v3

    int-to-float v2, v2

    mul-float/2addr v2, v8

    cmpl-float v0, v0, v2

    if-lez v0, :cond_6

    .line 1390
    iput v9, p0, Lcom/android/browser/WebGLAnimator;->mSnapScrollMode:I

    .line 1391
    if-lez v1, :cond_5

    move v0, v7

    :goto_3
    iput-boolean v0, p0, Lcom/android/browser/WebGLAnimator;->mSnapPositive:Z

    goto :goto_2

    :cond_5
    move v0, v6

    goto :goto_3

    .line 1393
    :cond_6
    iput v6, p0, Lcom/android/browser/WebGLAnimator;->mSnapScrollMode:I

    goto :goto_2

    .line 1348
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x40 -> :sswitch_1
    .end sparse-switch
.end method
